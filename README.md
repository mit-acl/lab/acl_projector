This is copied from the `boeing_demo_2018` repo and have been modified for multi-agent information gathering experiments. Please check [the wiki page](https://gitlab.com/mit-acl/infoplanner/acl_projector/-/wikis/home) for general info about the projector system, how to use the projector, and if needed, how to properly configure the projector system from scratch. 
**This does assume that the Scalable Display software can still run and retains the calibration for 8 projectors from the past.**

This repo should be cloned on THOR (the projector PC) and used for calibration and rendering.
There exists a file `C:\acl_projector\src\calibration.py` on THOR that may have different content compared to
what is in the repo.

Plotting libraries used on THOR: vispy==0.4.0, matplotlib==1.4.1, numpy==1.14.2

### Files ###

**projutil.py**: Last changed 2018-03-30. 
It consists of a `PosConverter` object that loads a pickle file with data used for linear interpolation among coordinates. 
(Unclear if still being used). Useful for converting between `proj_pos` and `vicon_pos`.

**ros_to_udp_node.py**: Last changed 2018-03-29 (commit msg: mocap wand calibration).
ROS node that converts ros messages to udp messages. For instance, `Wand/world` pose message or detection messages from
robot's measurement.


**projector_calibration.py**: Last changed 2018-03-30.
Should be run on THOR (projector PC) in order to project grid in highbay and collect corresponding Vicon positions.
Requires `ros_to_udp_node.py` and `wand_to_measurement.py` (no longer useful) running in ROS on some Linux PC.


**calibration_data_half_bay.p**: This is what's used by `boeing_projector.py`. Likely useful.


**projector_test.py**: Last changed 2018-03-30.

**projector_predemo.py**: Last changed 2018-03-30.

**boeing_projector.py**: Last changed 2018-04-04 (commit msg: reduce animation time).

**cali_data_fix.py**: Last changed 2018-03-30.



**udputil.py**: Last changed 2018-04-01.
**udp_talker.py**: Last changed 2018-02-22.
Keeps sending some messages given IP and port. Likley used for testing.
**udp_listener.py**: Last changed 2018-02-22.
Simply print the udp messages given IP and port. Likely used for testing.


**udp_sender.py**: Last changed 2018-04-03.
Likely not useful as it uses DOS-related messages.

**ros_talker.py**: Last changed 2018-02-22.

**wand_to_measurement.py**: Last changed 2018-02-22.
No longer useful, since `/Wand/world` is realdy a `PoseStamped` message.

**config.py**: Last changed 2018-04-04 (commit msg: reduce animation time).
**textobj.py**: Last changed 2018-04-03 (commit msg: displays classifier texts).
**visobj.py**: Last changed 2018-04-03 (commit msg: displays classifier texts).
**visobj_old.py**: Last changed 2018-03-30.







**projector.py**: Last changed 2018-03-06 (commit msg: add test). *Likely outdated*.
Needs a library called `gridmaster` but does not exist in the repo. 
**calibration_data_full_bay_fixed.backup**:
**calibration_data_full_bay.backup**:
**calibration_data_fixed.p**:
**calibration_data.p**:


# DESCRIPTION #

This repository provides necessary files for calibrating and running the ACL Projector system. The calibration scripts should be run from the projector computer (Thor).

### SETUP ###

#### Configure the `projector_calibration.py` file

* Find the IP address of the projector computer. If unsure how to do so, run the following line.

```ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'```

* Ensure that the computer's IP address matches the value in the python file.
```
IP_ADDRESS = "##.##.#.###"
```
* Set the size of the projector. 
```
PROJ_SIZE = (2400,3840) # Left half of Building 31 High Bay
PROJ_SIZE = (4800,3840) # All of Building 31 High Bay
```

#### Configure the `projector_calibration.launch` file

* Set the IP address found before as a launch parameter
```
 <param name="~host" value="##.##.#.#"/>
```

### Projector Calibration ###
* On the Linux/ROS computer, run the launch file
```
roslaunch projector_calibration.launch
```
* On the projector computer, run `projector_calibration.py`
```
python projector_calibration.py
```
* If the information is being sent correctly, you should see the Wand's location (in Vicon coordinates) being displayed in the top left corner of the projected image.
* Line up the Wand with the circled cross on the ground, and left-click on the projector computer to advance to the next circle
	* If you want to go back, perform a right-click
* When all the circles have been completed, the calibration data will be stored in a pickle file named `calibration_data.pickle`

### CONTACT ###
* Xiaoyi (Jeremy) Cai (xyc@mit.edu)
* Dong-Ki Kim (dkkim93@mit.edu)
* Samir Wadhwania (samirw@mit.edu)
