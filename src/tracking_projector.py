from __future__ import division
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.use("TkAgg")
mpl.rcParams['toolbar'] = 'None'  # Disable toolbar
import numpy as np
from matplotlib.patches import Circle, Ellipse
from matplotlib import transforms, image
from signal import signal, SIGINT
from os.path import dirname, abspath, join
from datetime import datetime
import udputil
import projutil

class TrackingPlotter(object):

    timer_interval_ms=50
    target_patch_size_m=0.6
    robot_patch_size_m=0.6
    target_zorder=5
    robot_zorder=4
    detection_zorder=3
    belief_mean_zorder=3
    belief_cov_zorder=2
    detection_timeout=3

    dpi=mpl.rcParams["figure.dpi"]

    def __init__(self, server_ip, server_port, proj_size_pixels=(2400, 3840), scale=1.0, backgroundcolor=(0.7,0.7,0.7)):
    # def __init__(self, server_ip, server_port, proj_size_pixels=(2400, 3840), scale=1.0, backgroundcolor=(1.0,1.0,1.0)):

        # Set up UDP functionalities
        self.server_ip = server_ip
        self.server_port = server_port
        self.udp = udputil.UdpReceiver(self.server_ip, self.server_port)

        # Canvas
        self.proj_size_pixels = proj_size_pixels  # (x axis, y axis)
        self.scale = scale
        self.size_in_inches = (self.scale*self.proj_size_pixels[0]/self.dpi, self.scale*self.proj_size_pixels[1]/self.dpi)
        self.fig, self.ax = plt.subplots(figsize=self.size_in_inches)
        self.ax.set_xlim([0,self.proj_size_pixels[0]])
        self.ax.set_ylim([0,self.proj_size_pixels[1]])

        self.ax.set_aspect('equal')
        # self.ax.invert_yaxis()

        # https://www.onooks.com/remove-all-whitespace-from-borders-in-matplotlib-imshow/
        # remove margins & all the white space surrounding axes
        plt.subplots_adjust(0,0,1,1,0,0)
        self.ax.margins(0,0)
        self.ax.xaxis.set_major_locator(plt.NullLocator())
        self.ax.yaxis.set_major_locator(plt.NullLocator())

        # Set the window location at top left for TkAgg: https://newbedev.com/how-do-you-set-the-absolute-position-of-figure-windows-with-matplotlib
        self.mngr = plt.get_current_fig_manager()
        self.mngr.window.wm_geometry("+0+0")

        # Remove window frame: https://www.py4u.net/discuss/221485
        self.fig.canvas.manager.window.overrideredirect(1)

        # Set canvas color
        self.ax.set_facecolor(backgroundcolor)

        # Position convertion
        # self.converter = projutil.PosConverter('calibration_data_half_bay.p')
        self.converter = projutil.PosConverter('calibration_left.p')
        self.xscale, self.yscale = self.converter.getUndistortFactors()

        # TODO: move to a config file / yaml file
        # Load target patches
        # Load images from assets folder TODO: remove hard-coding
        self.colors = ["b", "g", "r", "c", "m"]
        # self.shapes = ["square","square","square","square","square"]
        self.ids = [1,2,3,4,5]
        self.models =  ["ColoredShape_b_3",
                        "ColoredShape_g_3",
                        "ColoredShape_r_3",
                        "ColoredShape_c_3",
                        "ColoredShape_m_3"]

        dir = dirname(dirname(abspath(__file__)))  # acl_projector directory
        target_patch_dir = join(dir, "target_patches")
        self.patches = [image.imread(join(target_patch_dir, "{}.png".format(model))) for model in self.models]
        self.target_groundtruth_xys = [[np.nan, np.nan]]*len(self.ids)
        self.patch_handles = []
        self.cov_handles = []
        for idx, p in enumerate(self.patches):
            patch_h, patch_w, _ = p.shape
            max_side = max(patch_h, patch_w)
            width_m = patch_w / max_side * self.target_patch_size_m  # width in pixels
            height_m = patch_h / max_side * self.target_patch_size_m  # height in pixels
            # Center the image around origin (in meters); send patch outside window via transform
            self.patch_handles.append(self.ax.imshow(p, extent=[-width_m/2, width_m/2, -height_m/2, height_m/2],
                                                     clip_on=True, transform=transforms.Affine2D().translate(1e4, 1e4),
                                                     zorder=self.target_zorder))
            # Covariance with dashed outline
            cir = Circle((0,0), 1, edgecolor=self.colors[idx], fill=False, linestyle='--', linewidth=6,
                         transform=self.get_undistort_trans(posx=1e4, posy=1e4),
                         zorder=self.belief_cov_zorder,
                         alpha=0.5)
            self.cov_handles.append(cir)
            self.ax.add_artist(cir)

        # Use scatter plot as the mean of the estimates
        self.mean_handles = self.ax.scatter([1e4]*len(self.patches), [1e4]*len(self.patches), s=300, c=self.colors, marker='x',
                                            zorder=self.belief_mean_zorder, alpha=1, linewidth=10)

        # Edges for showing detections, constructed as measurements come in
        self.detection_lines = {}
        self.detection_lines_timeout_counter = {}

        # Get robot patches
        robot_patch_dir = join(dir, "robot_patches")
        self.uav_patch = image.imread(join(robot_patch_dir, "uav_k.png"))
        self.ugv_patch = image.imread(join(robot_patch_dir, "ugv_tri_right.png"))
        self.robot_patch_handles = dict()  # To be initialized upon receiving messages
        self.known_uav_types = ['HX', 'SQ']
        self.known_ugv_types = ['TB']

        # Create a new timer object. Set the interval to 100 milliseconds
        # (1000 is default) and tell the timer what function should be called.
        self.timer = self.fig.canvas.new_timer(interval=self.timer_interval_ms)
        self.timer.add_callback(self.update)


        self.sig_handle = signal(SIGINT, self.interrupt_shutdown)

    def start(self):
        self.timer.start()
        # Have to be after timer start() for some reason..
        plt.show()

    def interrupt_shutdown(self, signal_received, frame):
        print("SIGINT or CTRL-C detected. Close fig and exit.")
        self.shutdown()

    def shutdown(self):
        if self.fig:
            plt.close(self.fig)
        print("Shutdown.")
        exit(0)


    def get_undistort_trans(self, posx, posy, theta=0):
        """
        posx: x position in pixels
        posy: y position in pixels
        theta: angle in radians
        """
        # Assume that the image patch is already centered around origin and its size is in meters (before undistortion)
        return transforms.Affine2D().rotate(theta)\
                    .scale(self.xscale, self.yscale)\
                    .translate(posx, posy) + self.ax.transData

    def get_undistort_trans_cov(self, posx, posy, theta, width, height):
        return transforms.Affine2D().scale(width, height).rotate(theta)\
                    .scale(self.xscale, self.yscale)\
                    .translate(posx, posy) + self.ax.transData

    def update(self):

        if self.udp.update():
            udpDict = self.udp.get_latest_data()

            ## Do not visualize robot pose
            # # Visualize robot poses
            # for name in udpDict:
            #     if "global_odom" in name:
            #         # print(name)
            #         udp_msg = udpDict[name].data
            #         xy = self.converter.toProjPos(udp_msg['pose']['position'][:2])
            #         yaw = udp_msg['pose']['orientation']
            #
            #         # Which robot was this?
            #         splitnames = name.split('/')  # ['', VEH, 'odom']
            #         assert len(splitnames)>=2
            #         veh = splitnames[1]
            #
            #         if veh not in self.robot_patch_handles:
            #             patch = None
            #             if any(vehtype in veh for vehtype in self.known_uav_types):
            #                 patch = self.uav_patch
            #                 patch_h, patch_w, _ = patch.shape
            #             elif any(vehtype in veh for vehtype in self.known_ugv_types):
            #                 patch = self.ugv_patch
            #                 patch_h, patch_w, _ = patch.shape
            #             else:
            #                 print("Unexpected vehcle type: {}. Exit".format(veh))
            #                 self.shutdown()
            #
            #             max_side = max(patch_h, patch_w)
            #             width_m = patch_w / max_side * self.robot_patch_size_m  # width in pixels
            #             height_m = patch_h / max_side * self.robot_patch_size_m  # height in pixels
            #             # Center the image around origin (in meters); send patch outside window via transform
            #             self.robot_patch_handles[veh] = self.ax.imshow(patch, extent=[-width_m/2, width_m/2, -height_m/2, height_m/2], clip_on=True,
            #                                                            transform=self.get_undistort_trans(xy[0], xy[1], theta=yaw),
            #                                                            zorder=self.robot_zorder)
            #         else:
            #             self.robot_patch_handles[veh].set_transform(self.get_undistort_trans(xy[0], xy[1], theta=yaw))

            # Visualize ground truth belief
            if "ground_truth_beliefs" in udpDict:
                udp_msg = udpDict['ground_truth_beliefs'].data
                for idx, target_id in enumerate(udp_msg['ids']):
                    target_xy = self.converter.toProjPos(udp_msg['beliefs'][idx]['state'][:2])
                    target_patch_handle = self.patch_handles[idx]
                    target_patch_handle.set_transform(self.get_undistort_trans(target_xy[0], target_xy[1], theta=0))

                    # Update ground truth target pose
                    # self.debug_target_patches[idx].set_transform(self.get_undistort_trans(target_xy[0], target_xy[1], theta=0))
                    self.target_groundtruth_xys[idx] = target_xy

            # Uncertainty belief via gaussian filter
            if "belief" in udpDict:
                udp_msg = udpDict['belief'].data
                means = self.mean_handles.get_offsets()
                for idx, target_id in enumerate(udp_msg['ids']):
                    means[idx] = self.converter.toProjPos(udp_msg['beliefs'][idx]['state'][:2])

                    # Belief cov
                    cov = udp_msg['beliefs'][idx]['covariance']
                    assert len(cov)==16, "Assume that covariance is 4x4, but received {} elements in total.".format(len(cov))
                    target_cov_xy = np.array([[cov[0], cov[1]],[cov[4], cov[5]]])

                    width, height, yaw = self.get_cov_width_height_yaw(target_cov_xy)
                    self.cov_handles[idx].set_transform(
                        self.get_undistort_trans_cov(means[idx][0], means[idx][1], yaw, width, height))

                self.mean_handles.set_offsets(means)

            ## Do not visualize measurement either. Do this in video
            # # Update new measurement edges
            # detected = []
            # for name in udpDict:
            #     if "_measurements" in name:
            #         udp_msg = udpDict[name].data
            #         target_id = udp_msg['data_association']
            #         robot_id = udp_msg['sensor_idx']
            #         if target_id not in self.ids:
            #             print("Target with id {} is not in the list of ids {}".format(target_id, self.ids))
            #             self.shudown()
            #         target_xy = self.target_groundtruth_xys[self.ids.index(target_id)]
            #         robot_xy = self.converter.toProjPos(udp_msg['pose']['pose']['position'][:2])
            #         if (robot_id, target_id) in self.detection_lines:
            #             self.detection_lines[robot_id, target_id][0].set_data([robot_xy[0], target_xy[0]],
            #                                                                        [robot_xy[1], target_xy[1]])
            #             # print("Updating line positions!")
            #         else:
            #             self.detection_lines[robot_id, target_id] = self.ax.plot([robot_xy[0], target_xy[0]],
            #                                                                         [robot_xy[1], target_xy[1]],
            #                                                                           zorder=self.detection_zorder,
            #                                                                           linewidth=7, linestyle=':', color='k' )
            #         detected.append((robot_id, target_id))
            #         self.detection_lines_timeout_counter[robot_id, target_id] = 0

            # # Update missing measurement edges (send outside the window)
            # for (ri, ti) in self.detection_lines:
            #     if (ri,ti) not in detected:
            #         self.detection_lines_timeout_counter[ri,ti] += 1
            #         if self.detection_lines_timeout_counter[ri,ti] > self.detection_timeout:
            #             self.detection_lines[ri, ti][0].set_data([1e4, 1e4], [1e4, 1e4])



            # Get all the "/???_measurements" messages

        self.ax.figure.canvas.draw()

    def get_cov_width_height_yaw(self, cov, confidence=0.99):
        """
        Draw a covariance ellipse around a particular pose.
        :param pose: The center of the ellipse.
        :param cov: The covariance matrix to plot.
        :param clr: The color to plot.
        :param confidence: The confidence on [0, 1].
        :return: The resulting patch added to the figure.
        """
        # For 2D confidence, we can use the inverse of Chi-Square with 2 DOF
        s = -2 * np.log(1 - confidence)  # Confidence Parameter: s
        eig_val, eig_vec = np.linalg.eig(cov)
        width = 2 * np.sqrt(s * eig_val[0])
        height = 2 * np.sqrt(s * eig_val[1])
        yaw = np.arctan(eig_vec[0][1] / eig_vec[0][0])

        return width, height, yaw


if __name__ == '__main__':
    # IP_ADDRESS = "18.47.0.217"
    # IP_PORT = 21569
    # IP_ADDRESS = "127.0.0.1"
    # IP_PORT = 21569

    # THOR
    IP_ADDRESS = "192.168.0.141"
    IP_PORT = 21569
    plotter = TrackingPlotter(server_ip=IP_ADDRESS, server_port=IP_PORT, scale=1.0)
    plotter.start()

