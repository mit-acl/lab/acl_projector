from __future__ import division
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.use("TkAgg")
mpl.rcParams['toolbar'] = 'None'  # Disable toolbar
import numpy as np
from matplotlib.patches import Circle
from matplotlib import transforms

from datetime import datetime
import udputil
import projutil


class TrackingPlotter(object):

    def __init__(self, server_ip, server_port, proj_size_pixels=(2400, 3840), dpi=mpl.rcParams["figure.dpi"], timer_interval_ms=50, scale=1.0):

        # Set up UDP functionalities
        self.server_ip = server_ip
        self.server_port = server_port
        self.udp = udputil.UdpReceiver(self.server_ip, self.server_port)

        # Canvas
        self.proj_size_pixels = proj_size_pixels  # (x axis, y axis)
        self.dpi = dpi
        self.scale = scale
        self.size_in_inches = (self.scale*self.proj_size_pixels[0]/self.dpi, self.scale*self.proj_size_pixels[1]/self.dpi)
        self.fig, self.ax = plt.subplots(figsize=self.size_in_inches)
        self.ax.set_xlim([0,self.proj_size_pixels[0]])
        self.ax.set_ylim([0,self.proj_size_pixels[1]])
        self.timer_interval_ms = timer_interval_ms

        self.ax.set_aspect('equal')
        # self.ax.invert_yaxis()

        # https://www.onooks.com/remove-all-whitespace-from-borders-in-matplotlib-imshow/
        # remove margins & all the white space surrounding axes
        plt.subplots_adjust(0,0,1,1,0,0)
        self.ax.margins(0,0)
        self.ax.xaxis.set_major_locator(plt.NullLocator())
        self.ax.yaxis.set_major_locator(plt.NullLocator())

        # Set the window location at top left for TkAgg: https://newbedev.com/how-do-you-set-the-absolute-position-of-figure-windows-with-matplotlib
        self.mngr = plt.get_current_fig_manager()
        self.mngr.window.wm_geometry("+0+0")

        # Remove window frame: https://www.py4u.net/discuss/221485
        self.fig.canvas.manager.window.overrideredirect(1)

        # Create a new timer object. Set the interval to 100 milliseconds
        # (1000 is default) and tell the timer what function should be called.
        self.timer = self.fig.canvas.new_timer(interval=self.timer_interval_ms)
        # self.timer.add_callback(update, self)
        self.timer.add_callback(self.update)

        self.timer.start()


        # Position convertion
        # self.converter = projutil.PosConverter('calibration_data_half_bay.p')
        self.converter = projutil.PosConverter('calibration_left.p')
        self.xscale, self.yscale = self.converter.getUndistortFactors()
        self.trans_undistort = lambda xpos, ypos, theta: transforms.Affine2D().scale(self.xscale, self.yscale).rotate(theta) + self.ax.transData

        # Test variables
        self.tt = np.linspace(0,2*np.pi, 100)
        # r = self.converter.meterToInch(0.5)
        
        countour_r_in_meter = 3.5
        self.xx = round(self.proj_size_pixels[0]/2) + countour_r_in_meter*np.cos(self.tt)*self.xscale
        self.yy = round(self.proj_size_pixels[1]/2) + countour_r_in_meter*np.sin(self.tt)*self.yscale
        self.ax.plot(self.xx, self.yy, linewidth=5)
        self.counter = 0
        r = 0.5
        self.circle = Circle((0,0), r ,color='r', zorder=2, transform=self.get_undistort_trans(posx=0,posy=0))
        self.ax.add_artist(self.circle)

        # Test wand
        self.wand = Circle((0, 0), r, color='g', zorder=3, transform=self.get_undistort_trans(posx=0,posy=0))
        self.ax.add_artist(self.wand)

        plt.show()

    def get_undistort_trans(self, posx, posy, theta=0):
        return transforms.Affine2D().scale(self.xscale, self.yscale).rotate(theta).translate(posx, posy) + self.ax.transData

    def update(self):
        # Test variables
        self.counter += 1
        idx = self.counter%len(self.tt)
        # self.circle.center = self.xx[idx], self.yy[idx]
        self.circle.set_transform(self.get_undistort_trans(posx=self.xx[idx], posy=self.yy[idx]))
        print("circle location: {}, {}".format(self.xx[idx], self.yy[idx]))


        # Listen for wand location
        if self.udp.update():
            udpDict = self.udp.get_latest_data()
            # if 'ground_truth_beliefs' in udpDict.keys():
            #     self.current_ma_beliefs = udpDict['ground_truth_beliefs'].data
            #     self.current_marker_pos[0,:] = self.converter.toProjPos(self.current_ma_beliefs['beliefs'][0]['state'][:2])
            # print(udpDict)
            if 'WAND' in udpDict.keys():
                udp_msg = udpDict['WAND'].data
                wand_xy = self.converter.toProjPos(udp_msg['pose']['position'][:2])
                # self.wand.center = wand_xy[0], wand_xy[1]
                self.wand.set_transform(self.get_undistort_trans(posx=wand_xy[0], posy=wand_xy[1]))
                print('wand location: {}'.format((wand_xy[0], wand_xy[1])))

            else:
                print("No wand data")

        self.ax.figure.canvas.draw()



if __name__ == '__main__':
    # IP_ADDRESS = "18.47.0.217"
    # IP_PORT = 21569
    # IP_ADDRESS = "127.0.0.1"
    # IP_PORT = 21569

    # THOR
    IP_ADDRESS = "192.168.0.141"
    IP_PORT = 21569
    plotter = TrackingPlotter(server_ip=IP_ADDRESS, server_port=IP_PORT, scale=1)

