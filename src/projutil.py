#!/usr/bin/env python
from __future__ import division

import numpy as np
import pickle
from scipy import interpolate

"""
Read from a pickle file with calibration data for linear interpolation.
Unclear if this is still used, and by which script.
"""


class PosConverter(object):
	def __init__(self,cali_filename):
		# Read the calibration data file
		cali_data = pickle.load(open(cali_filename,'r'))
		# Initialize data
		self.proj_grid = cali_data['proj_grid']
		self.vicon_grid = cali_data['vicon_grid']
		self.window_size = cali_data['size']

		# Construct the linear interpolator
		self._to_vicon_x = interpolate.LinearNDInterpolator(self.proj_grid,self.vicon_grid[:,0])
		self._to_vicon_y = interpolate.LinearNDInterpolator(self.proj_grid,self.vicon_grid[:,1])

		self._to_proj_x = interpolate.LinearNDInterpolator(self.vicon_grid,self.proj_grid[:,0])
		self._to_proj_y = interpolate.LinearNDInterpolator(self.vicon_grid,self.proj_grid[:,1])

	def toViconPos(self,proj_pos):
		if proj_pos.ndim == 1:
			proj_pos = [proj_pos]

		proj_pos_x = self._to_vicon_x(proj_pos)
		proj_pos_y = self._to_vicon_y(proj_pos)

		return np.array([proj_pos_x[0],proj_pos_y[0]])

	def toProjPos(self,vicon_pos):
		if vicon_pos.ndim == 1:
			vicon_pos = [vicon_pos]

		vicon_pos_x = self._to_proj_x(vicon_pos)
		vicon_pos_y = self._to_proj_y(vicon_pos)
		
		return np.array([vicon_pos_x[0],vicon_pos_y[0]])

	def getUndistortFactors(self):
		"""Return pixel_size/vicon_size in x and y"""
		proj_min_x = np.min(self.proj_grid[:, 0])
		proj_min_y = np.min(self.proj_grid[:, 1])
		proj_max_x = np.max(self.proj_grid[:, 0])
		proj_max_y = np.max(self.proj_grid[:, 1])
		vicon_min_x = np.min(self.vicon_grid[:, 0])
		vicon_min_y = np.min(self.vicon_grid[:, 1])
		vicon_max_x = np.max(self.vicon_grid[:, 0])
		vicon_max_y = np.max(self.vicon_grid[:, 1])

		return (proj_max_x-proj_min_x)/(vicon_max_x-vicon_min_x), (proj_max_y-proj_min_y)/(vicon_max_y-vicon_min_y)

	def inchToMeter(self, inch):
		return inch*0.0254

	def meterToInch(self, meter):
		return meter/0.0254

	def inchToPixel(self, inch, dpi):
		return inch*dpi



