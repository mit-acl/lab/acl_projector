import numpy as np
import time
import sys
import collections
import math

from vispy import app, gloo, visuals
from vispy.visuals import transforms

import udputil
import projutil
import visobj
import gridmaster as gm


# Setup additional visual elements
udp_id_dict = {
    'mouse':('circle',{'center':[100,100],'color':[0,1,0,1]}),
}

class Projector(app.Canvas):
    def __init__(self,canvas_size,grid_size,host,port):
        app.Canvas.__init__(self, keys='interactive',size=canvas_size,position=(0.,0.),decorate=False,autoswap=True)
        # Set background color
        gloo.set_clear_color([1,1,1,1])
    
        # UDP data management
        self.udp = udputil.UdpReceiver(host,port)

        # Dict of visualizatoin objects
        self.visObjs = collections.OrderedDict()

        # Initialize additonal visobj
        for udp_id_key in udp_id_dict.keys():
            udp_dict_item = udp_id_dict[udp_id_key]
            udp_vistype = udp_dict_item[0]
            udp_kwag = udp_dict_item[1]
            self.visObjs[udp_id_key] = visobj.VisObj.creat_visobj(self,udp_vistype,**udp_kwag)

        # Initialize gridmaster
        self.grid = gm.GridMaster(self.size,grid_size)
        self.visObjs['grid'] = self.grid.gen_img_visobj(self)

        # For tracking framerate
        self.last_draw = time.time()

        # For rotation
        self.tick = 0.0

        # Link timer to callback funciton
        self._timer = app.Timer(1/60.0,connect=self.on_timer, start=True)

    def on_timer(self,event):
        
        # Handle udp data
        if self.udp.update():
            # If udp got new data, then grab them
            udp_data = self.udp.get_latest_data()
            self.handle_udp_data(udp_data)

            # Get all new data
            # udp_data = self.udp.get_new_data()
            # self.handle_udp_data(udp_data)

        # This tells the canvas to consider a redraw
        self.update()

    def handle_udp_data(self,udp_data):
        keys = udp_data.keys() 

        if 'grid' in keys:
            # data = udp_data['grid'].data
            # self.grid.set_color(data[0],data[1])
            data = udp_data['grid']
            for elem in data:
                self.grid.set_color(elem.data[0],elem.data[1])

        # Handling receiving the entire map in the beginning
        if 'whole_map' in keys:
            data = udp_data['whole_map'].data
            self.grid.set_color(range(len(data)), data)

    def on_mouse_move(self, event):
        # Have the 'mouse' visobj track the mouse movement
        pos = event.pos
        mouse = self.visObjs['mouse']
        mouse.set_properties(center=np.array(pos))
        mouse.set_properties(color=self.grid.get_closest_grid_color(pos))
        mouse.update_visuals()

    def on_key_press(self, event):
        if event.text == 'r':
            index_list = range(self.grid.num_of_grid)
            color_list = ['r','g','b','w','k']
            self.grid.set_color(index_list,np.random.choice(['r','g','b','w','k'],len(index_list)))

    def on_resize(self, event):
        # Handle resizing
        width, height = event.size
        gloo.set_viewport(0, 0, width, height)

    def on_draw(self, event):
        # This happens when a redraw is triggered

        # Clean the canvas
        gloo.clear(color=True,depth=True)

        # Go through all visobjs and call draw() on them
        for item in self.visObjs.items():
            item[1].draw()

        # Print
        now = time.time()
        # print 'Est. FPS %s' %(1.0/(now - self.last_draw))
        self.last_draw = now

if __name__ == '__main__':

    projector = Projector(canvas_size=[2400,3840],grid_size=[24,10],host="18.47.0.217",port=21569)
    # projector = Projector(canvas_size=[800,600],grid_size=[24,10],host='127.0.0.1',port=21569)
    projector.show()
    app.run()