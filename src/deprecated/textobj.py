import time
from vispy import visuals
from vispy.visuals import transforms
from vispy.color import Color

class TextObj():
    def __init__(self, canvas, text = 'Default', color = 'black', font_size = 72, pos = [0, 0]):
        self.text = visuals.TextVisual("", bold = True, rotation = 270)
        self.text.text = text
        self.text.color = color
        self.text.font_size = font_size
        self.text.pos = pos[0], pos[1]
        self.text_tr = visuals.transforms.TransformSystem(canvas)
        
        self.text_time = time.time() # represents when a text was last received

    def set_text(self, new_text):
        self.text.text = new_text

    def set_color(self, new_color):
        self.text.color = new_color

    def set_font_size(self, new_font_size):
        self.text.font_size = new_font_size

    def set_pos(self, new_pos):
        self.text.pos = new_pos

    def set_text_time(self, new_text_time):
        self.text_time = new_text_time

    def draw(self):
        self.text.draw(self.text_tr)

