#!/usr/bin/env python

import time
import sys
import collections
import math
import udputil
import projutil
import visobj
import numpy as np
from vispy import app, gloo, visuals
from vispy.visuals import transforms
from vispy import scene
from vispy.io import read_png, imread


# Setup additional visual elements
udp_id_dict = {
    'mouse':('circle',{'center':[100,100],'color':[0,1,0,1]}),
}

class Projector(app.Canvas):
    # Set global variables

    def __init__(self):
        # Set hyperparameters
        self.IP_ADDRESS = "18.47.0.217"
        self.IP_PORTS   = [21569, 21542, 21548, 21592, 21501, 21582, 21529, 21578]
        self.PROJ_SIZE  = (2400, 3840)
        self.COLORS     = [[1,0,0,1], [0,1,0,1], [0,0,1,1], [1,1,0,1], [1,0,1,1], [0,0,0,1]]
        self.COLORS.reverse()

        # Set Canvas
        # Ref: http://vispy.org/app.html
        self.canvas = app.Canvas.__init__(self, keys='interactive', size=self.PROJ_SIZE,
                                          position=(0., 0.), decorate=False, autoswap=True)
        gloo.set_clear_color([0.1, 0.1, 0.1, 1]) # r, g, b, alpha

        # Set image assets
        print '[ INFO ] Reading image assets'
        self.img_bg, self.img_bg_tr = self.import_img('assets/kitti_bg.png', fill_canvas=True)
        self.img_dest1, self.img_dest1_tr = self.import_img('assets/texture_D1.png', scale=[0.3, 0.4])
        self.img_dest2, self.img_dest2_tr = self.import_img('assets/texture_D2.png', scale=[0.3, 0.4])
        self.img_dest3, self.img_dest3_tr = self.import_img('assets/texture_D3.png', scale=[0.3, 0.4])

        # Import Vicon to Projector calibration data
        self.converter = projutil.PosConverter('calibration_data.p')
    
        # UDP data management
        self.vehicle_counter_receiver_port = self.IP_PORTS.pop()
        self.vehicle_counter_receiver = udputil.UdpReceiver(self.IP_ADDRESS,self.vehicle_counter_receiver_port)

        # Dict of visualization objects & receivers
        self.visObjs  = collections.OrderedDict()
        self.vehicles = collections.OrderedDict()

        # Wait for vehicle count, and initialize vehicles
        self.vehicles_received = False
        while not self.vehicles_received:
            if self.vehicle_counter_receiver.update():
                udpDict = self.vehicle_counter_receiver.get_latest_data()
                if 'VEH' in udpDict.keys():
                    self.vehicle_names = udpDict['VEH'].data
                    self.initalize_vehicles()
                    self.vehicles_received = True
                    print "[ INFO ] Vehicles received:", self.vehicle_names
                else:
                    print "No vehicles detected"

        # For rotation
        self.tick = 0.0

        # Link timer to callback funciton
        self._timer = app.Timer(1/60.0,connect=self.on_timer, start=True)

    def initalize_vehicles(self):
        for vehicle in self.vehicle_names:
            # Create receiver for each vehicle
            vehicle_port = self.IP_PORTS.pop()
            vehicle_reciever = udputil.UdpReceiver(self.IP_ADDRESS, vehicle_port)

            # Store receivers
            self.vehicles[vehicle] = vehicle_reciever

            # Store position and goal markers
            color = self.COLORS.pop() # Assign different colors for each agent
            pos_opts = {'center': [100, 100], 'color': color}
            goal_opts = {'center': [100, 100], 'color': color, 
                         'width':100, 'height':100}
            self.visObjs[vehicle+'loc']  = visobj.VisObj.creat_visobj(self, 'circle', **pos_opts)
            self.visObjs[vehicle+'goal'] = visobj.VisObj.creat_visobj(self, 'rect', **goal_opts)

    def preprocess_img(self, img):
        # Make transparent background
        return img

    def import_img(self, img_path, fill_canvas=False, scale=[1, 1], 
                   translate=[0,0], rotate_deg=0):
        img = imread(img_path)
        if not fill_canvas:
            img = self.preprocess_img(img)
        img = visuals.ImageVisual(img)

        # Apply transformation
        if fill_canvas:
            scale     = [1, 1]
            translate = [0, 0]

        img_tr = transforms.TransformSystem(self)
        aTrans = transforms.AffineTransform()
        aTrans.rotate(rotate_deg,(0, 0, 1))
        aTrans.scale(scale)
        aTrans.translate(translate)
        img_tr.visual_to_document = aTrans

        return img, img_tr

    def on_timer(self,event):
        # Check ports for new data
        for vehicle in self.vehicles.items():
            receiver = vehicle[1]
            if receiver.update():
                udp_data = receiver.get_latest_data()
                self.handle_udp_data(udp_data, vehicle[0])

        # This tells the canvas to consider a redraw
        self.update()

    def choose_dest(self, vehicle):
        # TODO Create img for each vehicle
        if int(vehicle[2:4]) == 1:
            return self.img_dest1_tr
        elif int(vehicle[2:4]) == 2:
            return self.img_dest2_tr
        elif int(vehicle[2:4]) == 3:
            return self.img_dest3_tr
        elif int(vehicle[2:4]) == 5:
            return self.img_dest1_tr
        elif int(vehicle[2:4]) == 7:
            return self.img_dest2_tr
        elif int(vehicle[2:4]) == 9:
            return self.img_dest3_tr

    def handle_udp_data(self, udp_data, vehicle):
        keys = udp_data.keys()

        if 'POS' in keys:
            pass # TODO 
        
        if 'GOAL' in keys:
            vicon_pos = udp_data['GOAL'].data
            proj_pos = self.converter.toProjPos(vicon_pos[:2])

            aTrans = transforms.AffineTransform()
            aTrans.scale([.3, .4])
            aTrans.translate(proj_pos)

            self.choose_dest(vehicle).visual_to_document = aTrans

        if 'VEL' in keys: 
            pass # TODO Add velocity

    def on_resize(self, event):
        width, height = event.size
        gloo.set_viewport(0, 0, width, height)

    def on_draw(self, event):
        gloo.clear(color=True, depth=True)

        # Draw background
        self.img_bg.draw(self.img_bg_tr)

        self.img_dest1.draw(self.img_dest1_tr)
        # self.img_dest2.draw(self.img_dest2_tr)
        self.img_dest3.draw(self.img_dest3_tr)

if __name__ == '__main__':
    projector = Projector()

    projector.show()
    app.run()