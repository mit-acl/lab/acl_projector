#!/usr/bin/env python
# license removed for brevity
import rospy

# Include the ROS msg to be send
from geometry_msgs.msg import Point
from geometry_msgs.msg import PoseStamped

import numpy as np
import math as math

def talker():
    
    rospy.init_node('talker', anonymous=True)
    
    pub_A = rospy.Publisher('/uP04/pose', PoseStamped, queue_size=10)
    pub_B = rospy.Publisher('/GP04/pose', PoseStamped, queue_size=10)

    hz = 60.0
    
    r = rospy.Rate(2)
    i = 0

    while not rospy.is_shutdown():
        # if i>100:
        #     i = 0

        msg_A = PoseStamped()
        msg_A.pose.position.x = 5*0.25*math.cos(math.pi*2*(i/hz)/2)
        msg_A.pose.position.y = 5*0.25*math.sin(math.pi*2*(i/hz)/2)
        msg_A.pose.position.z = 0
        msg_A.header.stamp = rospy.get_rostime()
        pub_A.publish(msg_A)

        print 'x', msg_A.pose.position.x
        print 'y', msg_A.pose.position.y

        msg_B = PoseStamped()
        msg_B.pose.position.x = 0.25*math.cos(-math.pi*2*(i/hz)/5)
        msg_B.pose.position.y = 0.25*math.sin(-math.pi*2*(i/hz)/5)
        msg_B.pose.position.z = 0;
        msg_B.header.stamp = rospy.get_rostime()
        pub_B.publish(msg_B)

        # y = y + 0.1*(np.random.random()-0.5)
        i = i+1
        r.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException: pass