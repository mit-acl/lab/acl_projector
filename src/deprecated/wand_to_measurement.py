#!/usr/bin/python
'''
Created on Feb 22, 2018

@author: Samir Wadhwania
'''
import rospy
from acl_msgs.msg import ViconState # TODO: replace msg
from geometry_msgs.msg import PoseStamped

# Actually this node is no longer useful since /Wand/world is already a PoseStamped message


"""
This script converges wand vicon measurement to `Wand/measurement` message consisting of pose

ViconState message is available here: https://bitbucket.org/jtorde/boeing_demo_2018/src/samir_dev/src/acl-system/acl_msgs/msg/ViconState.msg
Essentially:
------------------------------------------------
Header header
geometry_msgs/Pose pose
geometry_msgs/Twist twist
geometry_msgs/Vector3 accel
bool has_pose
bool has_twist
bool has_accel
------------------------------------------------

"""


class WandToMeasurement(object):
	def __init__(self):
		self.subscriber = rospy.Subscriber('Wand/vicon', ViconState, self.publish_measurement)
		self.publisher = rospy.Publisher('Wand/measurement', PoseStamped, queue_size=10)

	def publish_measurement(self, data):
		header = data.header
		pose = data.pose

		measurement = PoseStamped()
		measurement.header = header
		measurement.pose = pose

		self.publisher.publish(measurement)

if __name__ == "__main__":
	rospy.init_node("wand_to_measurement")
	try:
		WandToMeasurement()
	except rospy.ROSInterruptException:
		rospy.shutdown()
	rospy.spin()