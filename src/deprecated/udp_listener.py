import udputil
import numpy as np
import time

if __name__=='__main__':
	print 'Start listening.'
	commObj = udputil.UdpReceiver('10.0.0.4',21569)

	while True:
		if commObj.update():
			print commObj.get_latest_data()
		else:
			print 'No updates'
		time.sleep(0.05)