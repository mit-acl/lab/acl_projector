#!/usr/bin/env python

import pickle
import numpy as np 
import matplotlib.pyplot as plt 

indices = [
	0, 1, 7, 8, 72
]

cali_data = pickle.load(open('calibration_data.p'))
vicon_pts = np.array(cali_data['vicon_grid'])
x = vicon_pts[:, 0]
y = vicon_pts[:, 1]

x_errors = []
y_errors = []

for index in indices:
	print "Index %i: (%f, %f)" % (index, x[index], y[index])

	if index == 0:
		y[index] += 0.1
	elif index == 1:
		y[index] = 5.65
	elif index == 7:
		x[index] = 6.72
		y[index] = 6.25
	elif index == 8:
		x[index] = 8.20225
		y[index] = 6.32646
	elif index == 72:
		x[index] = 7.62125
		y[index] = -3.8

	x_errors.append(x[index])
	y_errors.append(y[index])

plt.scatter(x,y)
plt.scatter(x_errors, y_errors, c="red")
plt.show()

filename = 'calibration_data_fixed.p'
pickle_dict = dict()
pickle_dict['size'] = cali_data['size']
pickle_dict['proj_grid'] = cali_data['proj_grid']
pickle_dict['vicon_grid'] = vicon_pts

# pickle.dump(pickle_dict, open(filename, "wb" ))
# print "Saved to %s" %(filename)