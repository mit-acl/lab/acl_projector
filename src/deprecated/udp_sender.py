#!/usr/bin/env python
import rospy
import collections
import time # For timestamp
import udputil # For using udputil to send data over UDP
import numpy as np
from geometry_msgs.msg import PoseStamped, Vector3
from acl_msgs.msg import ViconState, VehicleList
from std_msgs.msg import Int8
from doa.msg import DOAData

class RosToUdp(object):
    """ 
    A ROS node to push the topic it subscribes 
    to through UDP to a specific host and port
    """ 
    def __init__(self, host, ports):
        # Initializing with host and ports of the destination receiver
        self.host  = host
        self.ports = ports
	
        # Track vehicles
        self.vehicles = collections.OrderedDict()
        self.vehicle_msg = None

        # Send vehicle init information
        self.counter = 0
        self.vehicle_counter = None
        self.vehicle_counter_port = None
        self.vehicle_counter_sender = None

        # Initialize the ROS node
        rospy.init_node('ros_to_udp', anonymous=False)
        
        # Listen for active vehicles
        try:
            self.vehicle_msg = rospy.wait_for_message("/doa_vehicles", VehicleList)
        except:
            print("[ INFO ] No vehicle message received")

        if self.vehicle_msg is not None:
            # Send vehicle names
            self.vehicle_counter = self.vehicle_msg.vehicle_names
            self.vehicle_counter_port = self.ports.pop()
            self.vehicle_counter_sender = udputil.UdpSender(self.host, self.vehicle_counter_port)
            self.vehicle_counter_sender.send('VEH', self.vehicle_counter, time.time())

            for vehicle in self.vehicle_msg.vehicle_names:
                # Assign open port to vehicle
                try:
                    port = self.ports.pop()
                except:
                    print("[ ERROR ] Out of ports to assign!")
                    break

                # Create Vehicle object
                self.vehicles[vehicle] = udputil.Vehicle(self.host, port)

                # Listen for incoming data about vehicle
                pos_topic = vehicle + '/vicon'
                rospy.Subscriber(pos_topic, ViconState, self.callback, (vehicle, 'POS'))

                doa_data_topic = vehicle + '/doa_data'
                rospy.Subscriber(doa_data_topic, DOAData, self.callback, (vehicle, 'DOA'))

                classifier_topic = vehicle + '/classifier'
                rospy.Subscriber(classifier_topic, Int8, self.callback, (vehicle, 'CLASSIFIER'), queue_size=10)
                
            	# Log
            	rospy.loginfo(vehicle + " assigned.")
        
        # Proper cleanup when the node shutdown
        rospy.on_shutdown(self.on_shutdown)

    def callback(self, data, (vehicle_id, data_type)):
    	# Periodically send init information
    	if self.counter % 30 == 0:
    	    self.vehicle_counter_sender.send('VEH', self.vehicle_counter, time.time())
    	self.counter += 1

        vehicle = self.vehicles[vehicle_id]

        if data_type == "POS":
            pos = np.array([data.pose.position.x, data.pose.position.y, data.pose.position.z])
            vehicle.update_position(pos)
            vehicle.send_position()
        elif data_type == "DOA":
            goal = np.array([data.goal.x, data.goal.y, data.goal_type.data])
            vel = np.array([data.doa_vel.x, data.doa_vel.y, data.doa_vel.z])
            vehicle.update_doa_data(goal, vel)
            vehicle.send_doa_data()
        elif data_type == "CLASSIFIER":
            print 'For vehicle {} received predicition {}'.format(vehicle_id, data.data)
            vehicle.update_classifier(data.data)
            vehicle.send_classifier()
        else:
            print "[ ERROR ] Unknown data type"

    def on_shutdown(self):
        pass

if __name__ == '__main__':
    # Start the node
    host = rospy.get_param("/host", "18.47.0.217")
    ports = rospy.get_param("/ports", [21569, 21542, 21548, 21592, 21501, 21582, 21529, 21578]) # TODO More ports depending on number of vehicles
    rosToUdp = RosToUdp(host, ports)

    # Keep it spinning to keep listening to the topics
    rospy.spin()
