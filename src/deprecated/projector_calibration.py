import numpy as np
import time
import sys
import pickle

import udputil

from vispy import app, gloo, visuals
from vispy.visuals import transforms

"""
This should be run on THOR (the projector PC).

Generate a 2d grid of points and measure their corresponding Vicon measurements.
Requires both ros_to_udp_node.py and wand_to_measurement.py running with correct IP and port numbers.
"""

class Canvas(app.Canvas):
    # Set local machine parameters
    IP_ADDRESS = "18.47.0.217"
    IP_PORT = 21569
    # PROJ_SIZE = (4800,3640) # TODO: wrong dimension? Half space is (2400, 3840)
    PROJ_SIZE = (2400,3840)

    def __init__(self):
        # Initialize canvas at top left corner of the screen
        # app.Canvas.__init__(self, keys='interactive',size=(4800,1920),position=(0.,0.),dpi=500.0,decorate=False)
        # app.Canvas.__init__(self, keys='interactive',size=(800,600),position=(0.,0.),dpi=500.0,decorate=False)
        app.Canvas.__init__(self, keys="interactive", size=self.PROJ_SIZE, position=(0.,0.), dpi=500.0, decorate=False)
        # Set background color
        gloo.set_clear_color((1,1,1,1))
        
        self.current_index = 0

        self.grid_pos_list = self.gen_grid_pos_list()
        self.vicon_pos_list = np.zeros(self.grid_pos_list.shape)
        
        self.tr_sys = transforms.TransformSystem(self)
        self.tr_sys.visual_to_document = transforms.STTransform()

        self.grid = visuals.MarkersVisual()
        self.grid.set_data(pos=self.grid_pos_list, face_color=[0.,0.,0.,1.0],symbol='cross',size=15.0)

        self.current_marker = visuals.MarkersVisual()
        self.current_marker_pos = np.zeros((1,2))
        self.current_marker_pos[0,:] = self.grid_pos_list[self.current_index]

        # self.current_marker.set_data(pos=self.current_marker_pos,symbol='ring',size='40.0',face_color=[0.,0.,0.,0.])
        self.current_marker.set_data(pos=self.current_marker_pos,symbol='ring',size=100.0,face_color='red')

        self.current_vicon_pos = np.zeros(2)

        # self.current_marker.set_data(pos=self.current_marker_pos,symbol='ring',size='40.0',face_color='red')

        self.text = visuals.TextVisual('',bold=True)
        self.text.text = '%s' %self.current_vicon_pos
        self.text.pos = [self.current_marker_pos[0,0],self.current_marker_pos[0,1]+50] # display text 50 pixels below
        self.text.font_size = 3.5

        self.udp = udputil.UdpReceiver(self.IP_ADDRESS, self.IP_PORT)

        # Bind timer
        self._timer = app.Timer('auto', connect=self.on_timer, start=True)

    def on_initialize(self,event):
        pass

    def on_resize(self, event):
        width, height = event.size
        gloo.set_viewport(0, 0, width, height)

    def on_draw(self, event):
        gloo.clear(color=True, depth=True)
        self.current_marker.draw(self.tr_sys)
        self.grid.draw(self.tr_sys)
        self.text.draw(self.tr_sys)

    def on_timer(self, event):
        # Receive the Wand 2d position and visualize on canvas
        if self.udp.update():
            udpDict = self.udp.get_latest_data()
            if 'WAND' in udpDict.keys():
                self.current_vicon_pos = udpDict['WAND'].data
                self.text.text = '%s' %self.current_vicon_pos
                self.update()
        # TODO grab Vicon position
        # self.update()

    def gen_grid_pos_list(self):
        # Return a list of grid points as a N-by-2 numpy array
        # Note that self.size is a property of parent class app.Canvas
        x_list = np.append(np.arange(0,self.size[0],self.size[0]/12.0),self.size[0]) # 12 points horizontally
        y_list = np.append(np.arange(0,self.size[1],self.size[1]/5.0),self.size[1]) # 5 points vertically
        x_grid,y_grid = np.meshgrid(x_list,y_list)
        pos_list = np.vstack([np.reshape(x_grid,x_grid.size),np.reshape(y_grid,y_grid.size)]).T
        return pos_list

    def save_and_quit(self):
        filename = 'calibration_data.p' # TODO: move this to top
        pickle_dict = dict()
        pickle_dict['size'] = self.size
        pickle_dict['proj_grid'] = self.grid_pos_list
        pickle_dict['vicon_grid'] = self.vicon_pos_list

        pickle.dump(pickle_dict, open(filename, "wb" ))
        print("Saved to {}".format(filename))
        self.close()

    def show_current_marker(self):
        self.current_marker_pos[0,:] = self.grid_pos_list[self.current_index]
        self.current_marker.set_data(pos=self.current_marker_pos,symbol='ring',size=100.0,face_color='red')
        self.text.pos = [self.current_marker_pos[0,0],self.current_marker_pos[0,1]+50]
        # self.text.text = '%s' %self.current_marker_pos
        self.update()
        # app.process_events()

    def on_mouse_press(self, event):
        
        if event.button == 1:
            # Left button
            self.show_current_marker()
            print("Marker Position: {}".format(self.current_marker_pos))
            print('Vicon Pos is: [{}, {}]'.format(self.current_vicon_pos[0],self.current_vicon_pos[1]))

            # self.vicon_pos_list = np.vstack([self.vicon_pos_list,self.current_vicon_pos])
            self.vicon_pos_list[self.current_index,:] = self.current_vicon_pos
            self.current_index = self.current_index+1

            if self.current_index < len(self.grid_pos_list):
                self.show_current_marker()
            else:
                self.save_and_quit()
        else:
            # Right button
            self.current_index = self.current_index - 1
            if self.current_index < 0:
                self.current_index = 0
            print("Rewind.")
            self.show_current_marker()

    def load_cali_data(self):
        return pickle.load(open('calibration_data.pickle','rb'))

if __name__ == '__main__':
    c = Canvas()
    c.show()
    app.run()
