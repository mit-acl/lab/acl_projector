import math
import time
import numpy as np 
from config import config
from vispy import visuals
from collections import OrderedDict
from vispy.io import imread
from vispy.visuals import transforms
from vispy.color import Color

class VisObj(object):
    @staticmethod
    def creat_visobj(canvas, visobj_type, **kwag):
        if visobj_type == 'circle':
            return CircleVisObj(canvas, **kwag)
        elif visobj_type == 'cross':
            return CrossVisObj(canvas, **kwag)
        elif visobj_type == 'arrow':
            return ArrowVisObj(canvas, **kwag)
        elif visobj_type == 'rect':
            return RectVisObj(canvas, **kwag)
        elif visobj_type == 'markers':
            return MarkersVisObj(canvas, **kwag)
        elif visobj_type == 'img':
            return ImageVisObj(canvas, **kwag)
        else:
            print '[VisObj.creat_visobj] visobj_type %s is not recognized.'
            return None

    def __init__(self, canvas):
        self.center = np.array([0.,0.])
        self.heading = 0.0
        self.scale = [1.0, 1.0]
        self.color = [1.0, 0.0, 0.0, 1.0]
        
        self._is_visible = True
        self._tr_sys = transforms.TransformSystem(canvas)
        self._tr_sys.visual_to_document = transforms.AffineTransform()
        self._tr_sys.visual_to_document.scale(self.scale)

        self.visuals = OrderedDict()


    def draw(self):
        if self._is_visible:
            self.update_visuals()

            for key in self.visuals.keys():
                self.visuals[key].draw(self._tr_sys)

    def set_visible(self, visible = True):
        self._is_visible = visible

    def set_properties(self, **kwag):
        self.__dict__.update(kwag)

    def update_visuals(self):
        pass
        # # Update the visuals according to data
        # tr_sys = self._tr_sys.visual_to_document
        # tr_sys.reset()
        # tr_sys.scale(self.scale)
        # # tr_sys.rotate(self.heading,[0, 0, 1])
        # tr_sys.translate([self.center[0], self.center[1], 0])

class CircleVisObj(VisObj):
    def __init__(self,canvas,**kwag):
        VisObj.__init__(self,canvas)

        # Set default 
        self.radius = 50        
        self.border_color = [1.,1.,1.,1.]
        self.center = [0.,0.]

        # Update _data
        self.set_properties(**kwag)

        # Create Visuals
        self.visuals['circle'] = visuals.EllipseVisual(pos=self.center,color=self.color,radius=self.radius,border_color=self.border_color)
    
    def update_visuals(self):
        VisObj.update_visuals(self)
        vis = self.visuals['circle']
        vis.pos = self.center
        vis.radius = self.radius
        vis.color = self.color
        vis.border_color = self.border_color

class CrossVisObj(VisObj):
    def __init__(self,canvas,**kwag):
        VisObj.__init__(self,canvas)

        self.radius = 50
        self.line_width = 10
        self.set_properties(**kwag)

        radius = self.radius

        self.visuals['vert'] = visuals.LineVisual(pos=np.array([[-radius/2.0,0.],[radius/2.0,0.]]),mode='agg',color=self.color,width=self.line_width)
        self.visuals['horz'] = visuals.LineVisual(pos=np.array([[0.,-radius/2.0],[0.,radius/2.0]]),mode='agg',color=self.color,width=self.line_width)
    
    def update_visuals(self):
        VisObj.update_visuals(self)
        radius = self.radius
        self.visuals['vert'].set_data(pos=np.array([[-radius/2.0,0.],[radius/2.0,0.]]),color=self.color,width=self.line_width)
        self.visuals['horz'].set_data(pos=np.array([[0.,-radius/2.0],[0.,radius/2.0]]),color=self.color,width=self.line_width)

class RectVisObj(VisObj):
    def __init__(self,canvas,**kwag):
        VisObj.__init__(self,canvas)

        self.width = 100 
        self.height = 50 
        self.pos = [0.,0.]
        self.radius = [0.,0.,0.,0.]
        self.border_color = [1.,1.,1.,1.]
        self.set_properties(**kwag)

        self.visuals['rect'] = visuals.RectangleVisual(pos=[0.,0.],
            height=self.height,
            width=self.width,
            radius=self.radius,
            color=self.color,
            border_color=self.border_color)

    def update_visuals(self):
        VisObj.update_visuals(self)
        rect = self.visuals['rect']
        rect.pos = self.pos
        rect.height = self.height
        rect.width = self.width
        rect.radius = self.radius
        rect.color = self.color
        rect.border_color = self.border_color

class ArrowVisObj(VisObj):
    def __init__(self,canvas,**kwag):
        VisObj.__init__(self,canvas)

        self.width = 100 
        self.pos = [[0.,0.],[0.,0.]]
        self.set_properties(**kwag)

        self.visuals['arrow'] = visuals.ArrowVisual()
            # pos=self.pos,
            # width=self.width,
            # color=self.color,
            # arrow_size=self.arrow_size)

    def update_visuals(self):
        VisObj.update_visuals(self)
        arrow = self.visuals['arrow']
        arrow.pos = self.pos
        arrow.width = self.width
        arrow.color = self.color
        arrow.arrow_size = self.arrow_size

class MarkersVisObj(VisObj):
    def __init__(self, canvas, **kwag):
        VisObj.__init__(self, canvas)
        self.pos = np.array([[0.,0.]])
        self.marker_size = config.VEL_TRAJ_SIZE
        self.edge_color = [0., 0., 0., 0.] # R, G, B, alpha
        self.edge_width = 1.0
        self.style = 'o'

        self.set_properties(**kwag)

        markers = visuals.MarkersVisual()
        markers.set_data(pos=self.pos, symbol=self.style, size=self.marker_size, face_color=self.color, 
                         edge_width=self.edge_width, edge_color=self.edge_color)
        self.visuals['markers'] = markers

    def update_visuals(self):
        VisObj.update_visuals(self)
        markers = self.visuals['markers']
        markers.set_data(pos=self.pos, symbol=self.style, size=self.marker_size, face_color=self.color,
                         edge_width=self.edge_width, edge_color=self.edge_color)

class ImageVisObj(VisObj):
    def __init__(self,canvas,**kwag):
        VisObj.__init__(self,canvas)
        self.img_data = np.random.uniform(0,255,(10,10,4)).astype(np.ubyte)
        self.set_properties(**kwag)
        
        visual = visuals.ImageVisual(self.img_data)
        self.visuals['grid'] = visual
    
    def update_visuals(self):
        VisObj.update_visuals(self)
        self.visuals['grid'].set_data(self.img_data)

class Quad(object):
    def __init__(self, canvas, center, color, radius, width):
        self.center = center
        self.color  = color
        self.canvas = canvas
        self.set_canvas_tr(canvas)

        # Set color map
        self.set_colormap()
        
        # Create the ring
        self.ring = Ring(color=color, radius=radius, width=width, method='agg')
        self.ring_outer = Ring(color='k', radius=radius+10, width=5, method='agg')
        self.ring_colors = np.random.uniform(0., 1., (self.ring._num_of_point, 4))

        # Set ring color ratio: ratio inside circle that decides color interpolation
        self.color_ratio = np.array([0.25, 0.25, 0.25])

        # For vehicle trajectory
        self.traj_pos = None
        self.set_trajectory()
        self.traj_tr = transforms.TransformSystem(self.canvas)
        self.traj_tr.visual_to_document = transforms.STTransform()

    def set_trajectory(self):
        # Set color property with alpha
        color_prop = self.color_map[self.color]
        color  = color_prop[:3]
        colors = np.full((config.VEL_TRAJ_NUM, 3), color)

        alpha = np.linspace(0.5, 0., config.VEL_TRAJ_NUM)
        alpha = np.array([alpha])
        colors = np.hstack((colors, alpha.T))

        self.traj_list = []
        for index in range(config.VEL_TRAJ_NUM):
            self.traj_list.append(Ring(color='k', radius=100, width=100, method='agg'))
            self.traj_list[index].set_color(np.full((self.traj_list[index]._num_of_point, 4), 
                                            colors[index, :]))

    def set_colormap(self):
        alpha = 0.9
        self.color_map = {
            'r': [1.0, 0.0, 0.0, alpha],
            'g': [0.0, 1.0, 0.0, alpha],
            'b': [0.0, 0.0, 1.0, alpha],
            'y': [1.0, 1.0, 0.0, alpha],
        }

        self.set_color_interp(alpha)

    def set_color_interp(self, alpha):
        if self.color == 'r':
            self.color_up   = [1.0, 0.25, 0.0, alpha]
            self.color_down = [1.0, 0.0, 0.25, alpha]
        elif self.color == 'g':
            self.color_up   = [0.25, 1.0, 0.0, alpha]
            self.color_down = [0.0, 1.0, 0.25, alpha]
        elif self.color == 'b':
            self.color_up   = [0.25, 0.0, 1.0, alpha]
            self.color_down = [0.0, 0.25, 1.0, alpha]
        else:
            RuntimeError('Undefined color')

    def set_canvas_tr(self, canvas):
        self.local_tr = transforms.TransformSystem(canvas)
        self.local_tr.visual_to_document = transforms.STTransform(translate=[self.center[0], self.center[1], -0.1])

    def set_center(self, center_pos):
        self.center = center_pos
        self.local_tr.visual_to_document.scale = [0.9, 1.2] # NOTE For fixing projector ratio 
        self.local_tr.visual_to_document.translate = [center_pos[0], center_pos[1], -0.1, 0]

    def update_ring_colors(self):
        num_of_point = self.ring._num_of_point
        vec = self.color_ratio
        vec_count = (num_of_point*(self.color_ratio/self.color_ratio.sum())).astype(int)

        # Set color code for interpolation
        color_codes = [self.color_map[self.color], self.color_up, self.color_down]
        color_list = list()
        for i in range(len(vec_count)):
            color_list.extend([color_codes[i]]*vec_count[i])

        if len(color_list) < num_of_point:
            color_list.extend([color_list[-1]]*(self.ring._num_of_point - len(color_list)))

        for i, color in enumerate(color_list):
            self.ring_colors[i, :] = color

        self.ring.set_color(self.ring_colors)

    def set_traj(self, traj_pos, traj_size):
        self.traj_pos  = traj_pos
        self.traj_size = traj_size/4.

        # Update trajectory with radius and width
        for index in range(config.VEL_TRAJ_NUM):
            self.traj_list[index].set_radius(self.traj_size[index])
            self.traj_list[index].set_width(self.traj_size[index]*2)
                
    def draw(self):
        self.ring.draw(self.local_tr)
        self.ring_outer.draw(self.local_tr)
        for index in range(config.VEL_TRAJ_NUM):
            if self.traj_pos is not None:
                self.traj_tr.visual_to_document.scale = [0.9, 1.2]
                self.traj_tr.visual_to_document.translate = self.traj_pos[index, :]
                self.traj_list[index].draw(self.traj_tr)

class Ring(object):
    def __init__(self, center=[0, 0], color='k', radius=50, width=5, method='gl', num_of_point=50):
        self._num_of_point = num_of_point # NOTE num_of_points define the quality of circle
        self._center       = np.array(center)
        self._min_radius   = radius
        self._radius       = self._min_radius
        self._rad_list     = math.pi*np.linspace(0, 2, self._num_of_point)
        self._dot_pos      = radius*np.array([[math.cos(rad), math.sin(rad)] for rad in self._rad_list])
        self._ring_colors  = color
        self._width        = width
        self._visual       = visuals.LineVisual(pos=self.get_points(), color=self._ring_colors, method=method, width=width, antialias=True)

    def set_center(self, center):
        self._center = np.array(center)
        self._visual.set_data(pos=self.get_points())

    def set_radius(self, radius):
        self._radius = radius
        self._dot_pos = self._radius*np.array([[math.cos(rad), math.sin(rad)] for rad in self._rad_list])
        self._visual.set_data(pos=self.get_points())

    def set_width(self, width):
        self._width = width
        self._visual.set_data(width=self._width)

    def set_color(self, color):
        self._ring_colors = color
        self._visual.set_data(color=self._ring_colors)
    
    def get_points(self):
        return (self._dot_pos + self._center).astype(np.float32)

    def get_radius(self):
        return self._radius

    def get_center(self):
        return self._center

    def draw(self, tr):
        self._visual.draw(tr)