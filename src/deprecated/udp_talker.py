# For using udputil to send data over UDP
import udputil
import numpy as np
import time

if __name__=='__main__':
	print 'Start talking.'
	pos = np.array([0,0])
	path = np.array([[-1.0,-1.0],[0.0,0.0],[1.0,1.0]])

	# commObj = udputil.UdpSender('128.31.7.50',21569)
	commObj = udputil.UdpSender('10.0.0.4',21569)

	color_map = ['r','g','b','w','k']
	index_list = range(12)

	while True:
		pos = pos + np.random.uniform(-0.05,0.05,2)
		now = time.time()
		# path = path + np.random.uniform(-0.05,0.05,path.shape)
		commObj.send("A",pos,now)
		commObj.send("B",pos + [10,10],now)
		color_code = np.random.choice(color_map,size=12)
		commObj.send("grid",(index_list,color_code),now)
		# commObj.send("GP03_PATH",path,time.time())
		time.sleep(0.3)
