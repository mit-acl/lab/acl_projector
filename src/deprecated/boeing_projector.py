#!/usr/bin/env python

import time
import sys
import math
import collections
import math
import udputil
import projutil
import numpy as np
import visobj

from config import config
from visobj import Quad
from textobj import TextObj
from vispy import app, gloo, visuals
from vispy.visuals import transforms
from vispy.io import read_png, imread
from vispy.color import Color



class Projector(app.Canvas):
    def __init__(self):
        app.Canvas.__init__(self, keys='interactive', size=config.PROJ_SIZE, 
        	                position=(0., 0.), decorate=False, autoswap=True)
        gloo.set_clear_color([0.1, 0.1, 0.1, 1]) # r, g, b, alpha

        # Setup calibration and assets
        self.converter = projutil.PosConverter('calibration_data_half_bay.p')
        self._setup_assets()

        # UDP data management
        self._setup_UDP()

        # Link timer to callback funciton
        self._timer = app.Timer(1/60., connect=self.on_timer, start=True)

        print '[ INFO ] Initialization complete'

    def _setup_assets(self):
        self.img_bg, self.img_bg_tr = self._import_img('assets/texture_bg.png', fill_canvas=True)
        # self.img_bg, self.img_bg_tr = self._import_img('assets/kitti.png', fill_canvas=False)

        # Class 0: vehicle
        self.img_vehicle_bad, self.img_vehicle_bad_tr = self._import_img('assets/dest_vehicle.png', 
                                                                         scale=config.GOAL_SCALE)

        # Class 1: foreset
        self.img_forest_bad, self.img_forest_bad_tr = self._import_img('assets/dest_forest.png', 
                                                                       scale=config.GOAL_SCALE)

        # Class 2: people
        self.img_people_bad, self.img_people_bad_tr   = self._import_img('assets/dest_people.png', 
                                                                         scale=config.GOAL_SCALE)
        self.img_people_help, self.img_people_help_tr = self._import_img('assets/pkg3_1.png', 
                                                                         scale=config.GOAL_SCALE)
       
    def _setup_UDP(self):
        self.vehicle_counter_receiver_port = config.IP_PORTS.pop()
        self.vehicle_counter_receiver = udputil.UdpReceiver(config.IP_ADDRESS, 
        	                                                self.vehicle_counter_receiver_port)

        self.visObjs  = collections.OrderedDict()
        self.vehicles = collections.OrderedDict()

        # Wait for vehicle count, and initialize vehicles
        self.vehicles_received = False
        while not self.vehicles_received:
            if self.vehicle_counter_receiver.update():
                udpDict = self.vehicle_counter_receiver.get_latest_data()
                if 'VEH' in udpDict.keys():
                    self.vehicle_names = udpDict['VEH'].data
                    self.initialize_vehicles()
                    self.vehicles_received = True
                    print "[ INFO ] Received vehicles {}".format(self.vehicle_names)

                else:
                    RuntimeError("[ ERROR ] No vehicles detected")

    def initialize_vehicles(self):
        print 'vehicle names', self.vehicle_names
        self.vehicle_labels = {} # maps vehicle name to list of pairs (text, conf) for classification output
        for vehicle in self.vehicle_names:
            # Create receiver for each vehicle
            vehicle_port = config.IP_PORTS.pop()
            vehicle_reciever = udputil.UdpReceiver(config.IP_ADDRESS, vehicle_port)

            # Store receivers
            self.vehicles[vehicle] = vehicle_reciever

            # For vehicle location and trajectory
            color = config.COLORS.pop() # Assign different colors for each agent
            self.visObjs[vehicle + 'loc'] = Quad(canvas=self, center=[0, 0], color=color, 
            	                                 radius=config.RING_RADIUS, width=config.RING_WIDTH)

            # For vehicle destination
            self.visObjs[vehicle + 'goal_vehicle_bad'], self.visObjs[vehicle + 'goal_vehicle_bad_tr'] = \
            self._import_img('assets/dest_vehicle.png', scale=config.GOAL_SCALE)
            self.visObjs[vehicle + 'goal_forest_bad'], self.visObjs[vehicle + 'goal_forest_bad_tr'] = \
            self._import_img('assets/dest_forest.png', scale=config.GOAL_SCALE)
            self.visObjs[vehicle + 'goal_people_bad'], self.visObjs[vehicle + 'goal_people_bad_tr'] = \
            self._import_img('assets/dest_people.png', scale=config.GOAL_SCALE)

            self.visObjs[vehicle + 'goal']                = None
            self.visObjs[vehicle + 'goal_tr']             = None             
            self.visObjs[vehicle + 'goal_type']           = None
            self.visObjs[vehicle + 'goal_vicon_pos']      = None
            self.visObjs[vehicle + 'goal_vicon_pos_last'] = None

            # For vehicle help
            self.visObjs[vehicle + 'help_vehicle'], self.visObjs[vehicle + 'help_vehicle_tr'] = \
            self._import_img('assets/help_vehicle.png', scale=config.HELP_SCALE)
            self.visObjs[vehicle + 'help_forest'], self.visObjs[vehicle + 'help_forest_tr'] = \
            self._import_img('assets/help_forest.png', scale=config.HELP_SCALE)
            self.visObjs[vehicle + 'help_people'], self.visObjs[vehicle + 'help_people_tr'] = \
            self._import_img('assets/pkg3_1.png', scale=config.HELP_SCALE)
            self.visObjs[vehicle + 'help_count'] = -1
            self.visObjs[vehicle + 'help_scale'] = config.HELP_SCALE
            self.visObjs[vehicle + 'draw_help']  = False

            # initialize vehicle classifier label text
            text = TextObj(self, text = '', pos =[0, 0])
            conf = TextObj(self, text = '', pos = [0 + text.text.font_size*1.5, 0])
            self.vehicle_labels[vehicle] = (text, conf)

    def _import_img(self, img_path, fill_canvas=False, scale=[1,1], translate=[0,0], rotate_deg=0):
        img_data = imread(img_path)
        img = visuals.ImageVisual(img_data)

        # Apply transformation
        if fill_canvas:
            scale = [config.PROJ_SIZE[0]/float(img.size[0]), 
                     config.PROJ_SIZE[1]/float(img.size[1])]
            translate = [0,0]
        
        img_tr = transforms.TransformSystem(self)
        aTrans = transforms.AffineTransform()
        aTrans.rotate(rotate_deg, (0,0,1))
        aTrans.scale(scale)
        aTrans.translate(translate)
        img_tr.visual_to_document = aTrans

        return img, img_tr

    def on_timer(self,event):
        for vehicle in self.vehicles.items():
            receiver = vehicle[1]

            if receiver.update():
                udp_data = receiver.get_latest_data()
                self.handle_udp_data(udp_data, vehicle[0])

        self.update()# This tells the canvas to consider a redraw

    def handle_udp_data(self, udp_data, vehicle):
        keys = udp_data.keys()

        if 'POS' in keys:
            vicon_pos = udp_data['POS'].data
            proj_pos = self.converter.toProjPos(vicon_pos[:2])

            # Set radius, which is inverse proportional to height
            radius = config.MAX_RING_RADIUS - config.RING_INV_RATE*vicon_pos[2]

            # Update ring position
            self.visObjs[vehicle + 'loc'].set_center(proj_pos)
            self.visObjs[vehicle + 'loc'].ring.set_radius(radius)
            self.visObjs[vehicle + 'loc'].ring_outer.set_radius(radius+10)

            # Update text location
            if vehicle + 'goal_proj_pos' in self.visObjs:
                class_pos = self.visObjs[vehicle + 'goal_proj_pos'] + [500, 300]
                self.vehicle_labels[vehicle][0].set_pos(class_pos)
                conf_pos = class_pos + [self.vehicle_labels[vehicle][0].text.font_size*1.5, 0]
                self.vehicle_labels[vehicle][1].set_pos(conf_pos)


        if 'GOAL' in keys:
            vicon_pos = udp_data['GOAL'].data
            proj_pos = self.converter.toProjPos(vicon_pos[:2])
            goal_type = vicon_pos[2]

            self.visObjs[vehicle + 'goal_type'] = goal_type
            self.visObjs[vehicle + 'goal'], self.visObjs[vehicle+'goal_tr'] = self.return_goal(goal_type, vehicle)
            self.visObjs[vehicle + 'goal_tr'] = self.update_goal_tr(proj_pos, goal_type, self.visObjs[vehicle+'goal'], 
                                                                    self.visObjs[vehicle+'goal_tr'], vehicle)
            self.visObjs[vehicle + 'goal_proj_pos'] = proj_pos

        if 'CLASSIFIER' in keys:
            help_type = udp_data['CLASSIFIER'].data
            self.visObjs[vehicle + 'help_type'] = help_type
            self.visObjs[vehicle + 'help'], self.visObjs[vehicle + 'help_tr'] = self.return_help(help_type, vehicle)
            self.visObjs[vehicle + 'help_scale'] = [0.60, 0.80] # Reinitialize
            self.visObjs[vehicle + 'help_count'] = config.HELP_COUNT # Reinitialize
            self.visObjs[vehicle + 'draw_help']  = True
            print 'For vehicle {} prediction {}'.format(vehicle, help_type)

        # if it has been over x seconds, clear the classifier text
        if time.time() - self.vehicle_labels[vehicle][0].text_time > 2.5:
            self.vehicle_labels[vehicle][0].set_text('')

        if 'VEL' in keys: 
            vicon_vel = udp_data['VEL'].data

            # Update trajectory
            traj_pos, traj_size = self.create_trajectory(vicon_vel, vehicle)
            self.visObjs[vehicle + 'loc'].set_traj(traj_pos, traj_size)

    def return_goal(self, goal_type, vehicle):
        if goal_type == 0:
            return self.visObjs[vehicle + 'goal_vehicle_bad'], self.visObjs[vehicle + 'goal_vehicle_bad_tr']
        elif goal_type == 1:
            return self.visObjs[vehicle + 'goal_forest_bad'], self.visObjs[vehicle + 'goal_forest_bad_tr']
        elif goal_type == 2:
        	return self.visObjs[vehicle + 'goal_people_bad'], self.visObjs[vehicle + 'goal_people_bad_tr']
        else:
        	RuntimeError("[ ERROR ] Unidentified goal type")

    def return_help(self, help_type, vehicle):
        # set the classifier time to be now
        self.vehicle_labels[vehicle][0].set_text_time(time.time())

        if help_type == 0:
            self.vehicle_labels[vehicle][0].set_text('Vehicle')
            return self.visObjs[vehicle + 'help_vehicle'], self.visObjs[vehicle + 'help_vehicle_tr']
        elif help_type == 1:
            self.vehicle_labels[vehicle][0].set_text('Forest')
            return self.visObjs[vehicle + 'help_forest'], self.visObjs[vehicle + 'help_forest_tr']
        elif help_type == 2:
            self.vehicle_labels[vehicle][0].set_text('People')
            return self.visObjs[vehicle + 'help_people'], self.visObjs[vehicle + 'help_people_tr']
        else:
            RuntimeError("[ ERROR ] Unidentified goal type")

    def update_goal_tr(self, proj_pos, goal_type, goal_img, tr, vehicle):
        # Calculate translation offset which is same as all type of images
        offset = [self.img_vehicle_bad.size[0]*config.GOAL_SCALE[0]/2., 
                  self.img_vehicle_bad.size[1]*config.GOAL_SCALE[1]/2.]
        proj_pos[0] -= offset[0]
        proj_pos[1] -= offset[1]

        # Update transformation
        aTrans = transforms.AffineTransform()
        # goal_type 2 has 3 times smaller image than others
        if goal_type == 2:
            aTrans.scale([config.GOAL_SCALE[0]*3, config.GOAL_SCALE[1]*3])
        else:
            aTrans.scale([config.GOAL_SCALE])
        aTrans.translate(proj_pos)
        tr.visual_to_document = aTrans

        self.visObjs[vehicle + 'goal_pos'] = proj_pos

        return tr

    def create_trajectory(self, vicon_vel, vehicle):
        # Get displacement of final marker from velocity 
        proj_vel_projection = vicon_vel[:2]
        proj_vel_projection[abs(proj_vel_projection) < 0.04] = 0 # ROS doesn't publish 0 velocity commands when agent stops moving
        proj_vel_projection *= config.VEL_SCALE # Scale factor
        proj_vel_projection[1] *= -1 # NOTE: Y coordinate in ROS if flipped in image coordinate system

        proj_z_vel = config.MAX_VEL - config.VEL_INV_RATE*vicon_vel[2]
        if proj_z_vel < 0:
            RuntimeError("[ ERROR ] Negative proj_z_vel detected")
            sys.exit()

        # Get vehicle data
        vehicle_pos = np.array(self.visObjs[vehicle + 'loc'].center)
        vehicle_height = self.visObjs[vehicle + 'loc'].ring.get_radius()

        # Final marker properties
        final_pos = vehicle_pos + proj_vel_projection
        final_height = vehicle_height + proj_z_vel

        # Create trajectory by doing interpolation
        x_pos = np.linspace(vehicle_pos[0], final_pos[0], config.VEL_TRAJ_NUM)
        y_pos = np.linspace(vehicle_pos[1], final_pos[1], config.VEL_TRAJ_NUM)
        traj_pos = np.column_stack((x_pos, y_pos))

        traj_size = np.linspace(vehicle_height, final_height, config.VEL_TRAJ_NUM)

        return traj_pos,traj_size

    def on_resize(self, event):
        width, height = event.size
        gloo.set_viewport(0, 0, width, height)
        
        self.marker_tr = transforms.TransformSystem(self)
        self.marker_tr.visual_to_document = transforms.NullTransform()

    def on_draw(self, event):
        gloo.clear(color=True, depth=True)

        # Background
        self.img_bg.draw(self.img_bg_tr)

        # Draw vehicle location and trajectory
        for vehicle in self.vehicle_names:
            self.visObjs[vehicle + 'loc'].update_ring_colors()
            self.visObjs[vehicle + 'loc'].draw()

        # Text
        for text, conf in self.vehicle_labels.values():
            text.draw()
            conf.draw()

        # Draw Goal
        for vehicle in self.vehicle_names:
            if self.visObjs[vehicle + 'goal_type'] is not None:
                self.visObjs[vehicle + 'goal'].draw(self.visObjs[vehicle + 'goal_tr'])

        # Draw help package
        for vehicle in self.vehicle_names:
            if self.visObjs[vehicle + 'draw_help']:
                if self.visObjs[vehicle + 'help_count'] >= 0:
                    self.draw_help(vehicle)
                else:
                    self.visObjs[vehicle + 'draw_help'] = False
                    self.visObjs[vehicle + 'help_scale'] = [0.60, 0.80] # Reinitialize

    def draw_help(self, vehicle): 
        ''' help_type indicates what type of help should be provided
        e.g. water --> forest fire
             aid packages --> dest_people
        draws a slowly decreasing image of the help (help package being delivered/dropped)
        '''
        self.visObjs[vehicle + 'help_count'] -= 1

        # Apply downscale for animation
        self.visObjs[vehicle + 'help_scale'][0] *= config.DOWNSCALE
        self.visObjs[vehicle + 'help_scale'][1] *= config.DOWNSCALE
        self.calculate_help_pos(vehicle, self.visObjs[vehicle + 'help'])

        # Draw
        aTrans = transforms.AffineTransform()
        aTrans.scale(self.visObjs[vehicle + 'help_scale'])
        aTrans.translate(self.visObjs[vehicle + 'help_pos']) # position of help package
        self.visObjs[vehicle + 'help_tr'].visual_to_document = aTrans

        self.visObjs[vehicle + 'help'].draw(self.visObjs[vehicle + 'help_tr'])

    def calculate_help_pos(self, vehicle, help_img):
        # Goal size is same regardless of any goal type
        goal_size = [self.img_vehicle_bad.size[0]*config.GOAL_SCALE[0], 
                     self.img_vehicle_bad.size[1]*config.GOAL_SCALE[1]]

        help_pos_x = self.visObjs[vehicle + 'goal_pos'][0] + goal_size[0]/2.0 - help_img.size[0]*self.visObjs[vehicle + 'help_scale'][0]/2.
        help_pos_y = self.visObjs[vehicle + 'goal_pos'][1] + goal_size[1]/2.0 - help_img.size[1]*self.visObjs[vehicle + 'help_scale'][1]/2.
        self.visObjs[vehicle + 'help_pos'] = [help_pos_x, help_pos_y]

if __name__ == '__main__':
    projector = Projector()
    projector.show()
    app.run()
