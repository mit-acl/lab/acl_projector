import numpy as np
import time
import sys
import pickle

import udputil
import projutil

from vispy import app, gloo, visuals
from vispy.visuals import transforms

class Canvas(app.Canvas):
    # Set local machine parameters
    IP_ADDRESS = "18.47.0.217"
    IP_PORT = 21569
    PROJ_SIZE = (2400,3840)

    def __init__(self):
        # Initialize canvas
        # app.Canvas.__init__(self, keys='interactive',size=(4800,1920),position=(0.,0.),dpi=500.0,decorate=False)
        app.Canvas.__init__(self, keys="interactive", size=self.PROJ_SIZE, position=(0.,0.), dpi=500.0, decorate=False)
        # Set background color
        gloo.set_clear_color((1,1,1,1))

        self.tr_sys = transforms.TransformSystem(self)
        self.tr_sys.visual_to_document = transforms.STTransform()

        self.converter = projutil.PosConverter('calibration_data.p')

        self.current_marker = visuals.MarkersVisual()
        self.current_marker_pos = np.zeros((1,2))

        self.current_marker.set_data(pos=self.current_marker_pos,symbol='ring',size=100.0,face_color='red')

        self.current_vicon_pos = np.zeros(2)

        self.udp = udputil.UdpReceiver(self.IP_ADDRESS, self.IP_PORT)

        # Bind timer
        self._timer = app.Timer('auto', connect=self.on_timer, start=True)

    def on_initialize(self,event):
        pass

    def on_resize(self, event):
        width, height = event.size
        gloo.set_viewport(0, 0, width, height)

    def on_draw(self, event):
        gloo.clear(color=True, depth=True)
        self.current_marker.draw(self.tr_sys)

    def on_timer(self, event):
        if self.udp.update():
            udpDict = self.udp.get_latest_data()
            if 'WAND' in udpDict.keys():
                self.current_vicon_pos = udpDict['WAND'].data
                self.current_marker_pos[0,:] = self.converter.toProjPos(self.current_vicon_pos)
                self.show_current_marker()
                self.update()

    def show_current_marker(self):
        self.current_marker.set_data(pos=self.current_marker_pos,symbol='ring',size=100.0,face_color='red')
        self.update()

if __name__ == '__main__':
    c = Canvas()
    c.show()
    app.run()
