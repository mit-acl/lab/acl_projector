from vispy import visuals
from vispy.visuals import transforms
from collections import OrderedDict
import numpy as np 

class VisObj(object):
    @staticmethod
    def creat_visobj(canvas,visobj_type,**kwag):
        if visobj_type == 'circle':
            return CircleVisObj(canvas,**kwag)
        elif visobj_type == 'cross':
            return CrossVisObj(canvas,**kwag)
        elif visobj_type == 'arrow':
            return ArrowVisObj(canvas,**kwag)
        elif visobj_type == 'rect':
            return RectVisObj(canvas,**kwag)
        elif visobj_type == 'markers':
            return MarkersVisObj(canvas,**kwag)
        elif visobj_type == 'img':
            return ImageVisObj(canvas,**kwag)
        else:
            print '[VisObj.creat_visobj] visobj_type %s is not recognized.'
            return None

    def __init__(self,canvas):
        self.center = np.array([0.,0.])
        self.heading = 0.0
        self.scale = [1.0,1.0]
        self.color = [1.0,0.0,0.0,1.0]
        
        self._is_visible = True
        self._tr_sys = transforms.TransformSystem(canvas)
        self._tr_sys.visual_to_document = transforms.AffineTransform()

        self.visuals = OrderedDict()

    def draw(self):
        if self._is_visible:
            # Update the visuals    
            self.update_visuals()
            # Draw them
            for key in self.visuals.keys():
                self.visuals[key].draw(self._tr_sys)

    def set_visible(self,visible = True):
        self._is_visible = visible

    def set_properties(self,**kwag):
        self.__dict__.update(kwag)

    def update_visuals(self):
        # Update the visuals according to data
        # pass
        tr_sys = self._tr_sys.visual_to_document
        tr_sys.reset()
        tr_sys.scale(self.scale)
        tr_sys.rotate(self.heading,[0,0,1])
        tr_sys.translate([self.center[0],self.center[1],0])


class CircleVisObj(VisObj):
    def __init__(self,canvas,**kwag):
        VisObj.__init__(self,canvas)

        # Set default 
        self.radius = 50        
        self.border_color = [1.,1.,1.,1.]

        # Update _data
        self.set_properties(**kwag)

        # Create Visuals
        self.visuals['circle'] = visuals.EllipseVisual(pos=[0.,0.],color=self.color,radius=self.radius,border_color=self.border_color)
    
    def update_visuals(self):
        VisObj.update_visuals(self)
        vis = self.visuals['circle']
        vis.radius = self.radius
        vis.color = self.color
        vis.border_color = self.border_color

class ArrowVisObj(VisObj):
    def __init__(self,canvas,**kwag):
        VisObj.__init__(self,canvas)

        self.radius = 50
        self.line_width = 10
        self.set_properties(**kwag)

        radius = self.radius

        self.visuals['vert'] = visuals.LineVisual(pos=np.array([[-radius/2.0,0.],[radius/2.0,0.]]),mode='agg',color=self.color,width=self.line_width)
        self.visuals['horz'] = visuals.LineVisual(pos=np.array([[0.,-radius/2.0],[0.,radius/2.0]]),mode='agg',color=self.color,width=self.line_width)
    
    def update_visuals(self):
        VisObj.update_visuals(self)
        radius = self.radius
        self.visuals['vert'].set_data(pos=np.array([[-radius/2.0,0.],[radius/2.0,0.]]),color=self.color,width=self.line_width)
        self.visuals['horz'].set_data(pos=np.array([[0.,-radius/2.0],[0.,radius/2.0]]),color=self.color,width=self.line_width)



class RectVisObj(VisObj):
    def __init__(self,canvas,**kwag):
        VisObj.__init__(self,canvas)

        self.width = 100 
        self.height = 50 
        self.radius = [0.,0.,0.,0.]
        self.border_color = [1.,1.,1.,1.]
        self.set_properties(**kwag)

        self.visuals['rect'] = visuals.RectangleVisual(pos=[0.,0.],
            height=self.height,
            width=self.width,
            radius=self.radius,
            color=self.color,
            border_color=self.border_color)

    def update_visuals(self):
        VisObj.update_visuals(self)
        rect = self.visuals['rect']
        rect.height = self.height
        rect.width = self.width
        rect.radius = self.radius
        rect.color = self.color
        rect.border_color = self.border_color

class MarkersVisObj(VisObj):
    def __init__(self,canvas,**kwag):
        VisObj.__init__(self,canvas)
        self.pos = np.array([[0.,0.]])
        self.marker_size = 50.0
        self.edge_color = [0.,0.,0.,1.]
        self.edge_width = 1.0
        self.style = 'o'

        self.set_properties(**kwag)

        markers = visuals.MarkersVisual()
        markers.set_data(pos=self.pos,size=self.marker_size,face_color=self.color,edge_width=self.edge_width,edge_color=self.edge_color)
        markers.set_style(self.style)
        self.visuals['markers'] = markers

    def update_visuals(self):
        VisObj.update_visuals(self)
        markers = self.visuals['markers']
        markers.set_data(pos=self.pos,size=self.marker_size,face_color=self.color,edge_width=self.edge_width,edge_color=self.edge_color)
        markers.set_style(self.style)

class ImageVisObj(VisObj):
    def __init__(self,canvas,**kwag):
        VisObj.__init__(self,canvas)
        self.img_data = np.random.uniform(0,255,(10,10,4)).astype(np.ubyte)
        self.set_properties(**kwag)
        
        visual = visuals.ImageVisual(self.img_data)
        self.visuals['grid'] = visual
    
    def update_visuals(self):
        VisObj.update_visuals(self)
        self.visuals['grid'].set_data(self.img_data)
