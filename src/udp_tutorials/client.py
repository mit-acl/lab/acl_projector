#!/usr/bin/env python
import socket

UDP_IP_ADDRESS = "10.29.85.123"
UDP_PORT_NO = 6789
BUFFER_SIZE = 1024
Message = "Hello, Server"
bytesToSend = Message.encode()

clientSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
clientSock.sendto(bytesToSend, (UDP_IP_ADDRESS, UDP_PORT_NO))

data, addr = clientSock.recvfrom(BUFFER_SIZE)
print("Received server msg: {}".format(data.decode()))
print("Server IP: {}".format(addr))