#!/usr/bin/env python
import rospy

# For using udputil to send data over UDP
import udputil
import numpy as np

# Include the ROS msgs to receive/send
from geometry_msgs.msg import PoseStamped, TwistStamped
from nav_msgs.msg import Odometry
from erl_msgs.msg import MultiDoubleIntegratorBelief, DoubleIntegratorBelief
from erl_sensing_ros.msg import RangeMeasurement
from tf.transformations import quaternion_from_euler, euler_from_quaternion
# For timestamp
import time

class RosToUdp(object):
    # A ROS node to push the topic it subscribes to through UDP to a specific host and port

    def __init__(self):
        self.host = rospy.get_param("~host")
        self.port = rospy.get_param("~port")
        self.belief_topic = rospy.get_param("~belief_topic", '/target_sim/ground_truth_beliefs')

        # Initializing with host and port of the destination
        self.udp_sender = udputil.UdpSender(self.host, self.port)
        # self.converter = projutil.PosConverter('calibration_data.pickle')

        self.all_vehs = rospy.get_param('~vehs', None)
        assert self.all_vehs is not None, "Cannot load robots to compute safety control for."

        self.topic_udp = {
            '/Wand/world':(PoseStamped,'WAND'),
            '/range_measurements': (RangeMeasurement, "range_measurements"),
            self.belief_topic: (MultiDoubleIntegratorBelief, "ground_truth_beliefs"),
            '/gaussian_filter/belief': (MultiDoubleIntegratorBelief, 'belief')
        }

        ## Do not subscribe to odom msg for visualization
        # for veh in self.all_vehs:
        #     self.topic_udp['/{}/ground_truth/global_odom'.format(veh)] = (Odometry, "/{}/ground_truth/global_odom".format(veh))
        
        for topic, (msg_type, udp_id) in self.topic_udp.items():
            rospy.Subscriber(topic,msg_type, self.callback, udp_id)

        # Proper cleanup when the node shutdown
        rospy.on_shutdown(self.on_shutdown)

    def callback(self, msg, udp_id):
 
        # Convert ros_msg to primitive data to be sent by udp
        udp_data = self.encode_ros_msg(msg)

        # udp_data = np.array(np.random.random(500))

        # rospy.loginfo(rospy.get_caller_id() + " udp_id: %s, udp_data %s",udp_id,udp_data)
        rospy.loginfo_throttle(2.0, "Msg sent by {}; udp_id:{}, udp_data:{}".format(rospy.get_caller_id(),udp_id,udp_data))
        # Send through udp
        self.udp_sender.send(udp_id,udp_data,time.time())

    def on_shutdown(self):
        pass


    def encode_ros_msg(self, msg):
        if isinstance(msg, MultiDoubleIntegratorBelief):
            udp_msg = {
                "ids": msg.ids,
                "beliefs": [self.encode_ros_msg(belief_msg) for belief_msg in msg.beliefs]
            }
        elif isinstance(msg, DoubleIntegratorBelief):
            udp_msg = {
                "state": np.asarray(msg.state),
                "covariance": np.asarray(msg.covariance)
            }
        elif isinstance(msg, PoseStamped):
            position = msg.pose.position
            ori = msg.pose.orientation
            udp_msg = {
                "pose": {
                    "position": np.asarray([position.x, position.y, position.z]),
                    "orientation": euler_from_quaternion((ori.x, ori.y, ori.z, ori.w), "szyx")[0],
                }
            }
        elif isinstance(msg, Odometry):
            position = msg.pose.pose.position
            ori = msg.pose.pose.orientation
            udp_msg = {
                "pose": {
                    "position": np.asarray([position.x, position.y, position.z]),
                    "orientation": euler_from_quaternion((ori.x, ori.y, ori.z, ori.w), "szyx")[0],
                }
            }
        elif isinstance(msg, RangeMeasurement):
            udp_msg = {
                "data_association": msg.data_association,
                "sensor_idx": msg.sensor_idx,
                "pose": self.encode_ros_msg(msg.pose)
            }
        else:
            rospy.logwarn_throttle(2.0, "[ros_to_udp] Message of type {} is not handled.".format(type(msg)))
            udp_msg = None
        return udp_msg

if __name__ == '__main__':
    # TODO: put things into param files and write launch file?
    # Star the node
    # host = rospy.get_param("~host")
    # port = rospy.get_param("~port")
    # host = '18.47.0.217'
    # port = 21569
    # rosToUdp = RosToUdp(host, port)

    # Initialize the ROS node
    rospy.init_node('ros_to_udp', anonymous=False)
    rosToUdp = RosToUdp()
    # Keep it spinning to keep listening to the topics
    rospy.spin()
