from __future__ import division
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.use("TkAgg")
mpl.rcParams['toolbar'] = 'None'  # Disable toolbar
import numpy as np
from matplotlib.patches import Circle, Ellipse
from matplotlib import transforms, image
from signal import signal, SIGINT
from os.path import dirname, abspath, join
from datetime import datetime
import udputil
import projutil

class TrackingPlotter(object):

    timer_interval_ms=50
    target_patch_size_m=0.6
    robot_patch_size_m=0.6
    target_zorder=5
    robot_zorder=4
    detection_zorder=3
    belief_mean_zorder=3
    belief_cov_zorder=2
    detection_timeout=3

    dpi=mpl.rcParams["figure.dpi"]

    # def __init__(self, server_ip, server_port, proj_size_pixels=(2400, 3840), scale=1.0, backgroundcolor=(0.7,0.7,0.7)):
    # def __init__(self, server_ip, server_port, proj_size_pixels=(2400, 3840), scale=1.0, backgroundcolor=(1.0,1.0,1.0)):
    def __init__(self, server_ip, server_port, proj_size_pixels=(2400, 3840), scale=1.0, backgroundcolor=(1.0,1.0,1.0)):

        # # Set up UDP functionalities
        # self.server_ip = server_ip
        # self.server_port = server_port
        # self.udp = udputil.UdpReceiver(self.server_ip, self.server_port)

        # Canvas
        self.proj_size_pixels = proj_size_pixels  # (x axis, y axis)
        self.scale = scale
        self.size_in_inches = (self.scale*self.proj_size_pixels[0]/self.dpi, self.scale*self.proj_size_pixels[1]/self.dpi)
        self.fig, self.ax = plt.subplots(figsize=self.size_in_inches)
        self.ax.set_xlim([0,self.proj_size_pixels[0]])
        self.ax.set_ylim([0,self.proj_size_pixels[1]])

        self.ax.set_aspect('equal')
        # self.ax.invert_yaxis()

        # https://www.onooks.com/remove-all-whitespace-from-borders-in-matplotlib-imshow/
        # remove margins & all the white space surrounding axes
        plt.subplots_adjust(0,0,1,1,0,0)
        self.ax.margins(0,0)
        self.ax.xaxis.set_major_locator(plt.NullLocator())
        self.ax.yaxis.set_major_locator(plt.NullLocator())

        # Set the window location at top left for TkAgg: https://newbedev.com/how-do-you-set-the-absolute-position-of-figure-windows-with-matplotlib
        self.mngr = plt.get_current_fig_manager()
        self.mngr.window.wm_geometry("+0+0")

        # Remove window frame: https://www.py4u.net/discuss/221485
        self.fig.canvas.manager.window.overrideredirect(1)

        # Set canvas color
        self.ax.set_facecolor(backgroundcolor)

        # Position convertion
        # self.converter = projutil.PosConverter('calibration_data_half_bay.p')
        self.converter = projutil.PosConverter('calibration_left.p')
        self.xscale, self.yscale = self.converter.getUndistortFactors()

        # TODO: move to a config file / yaml file
        # Load target patches
        # Load images from assets folder TODO: remove hard-coding
        self.colors = ["b", "g", "r", "c", "m"]
        # self.shapes = ["square","square","square","square","square"]
        self.ids = [1,2,3,4,5]
        self.models =  ["ColoredShape_b_3",
                        "ColoredShape_g_3",
                        "ColoredShape_r_3",
                        "ColoredShape_c_3",
                        "ColoredShape_m_3"]

        # fixed target locations.
        self.fixed_xy_positions = [(-2,-2), (-2,2), (2,-2), (2,2), (0,0)]

        dir = dirname(dirname(abspath(__file__)))  # acl_projector directory
        target_patch_dir = join(dir, "target_patches")
        self.patches = [image.imread(join(target_patch_dir, "{}.png".format(model))) for model in self.models]
        self.target_groundtruth_xys = [[np.nan, np.nan]]*len(self.ids)
        self.patch_handles = []
        self.cov_handles = []
        for idx, p in enumerate(self.patches):
            patch_h, patch_w, _ = p.shape
            max_side = max(patch_h, patch_w)
            width_m = patch_w / max_side * self.target_patch_size_m  # width in pixels
            height_m = patch_h / max_side * self.target_patch_size_m  # height in pixels
            # Center the image around origin (in meters); send patch outside window via transform
            self.patch_handles.append(self.ax.imshow(p, extent=[-width_m/2, width_m/2, -height_m/2, height_m/2],
                                                     clip_on=True, transform=transforms.Affine2D().translate(1e4, 1e4),
                                                     zorder=self.target_zorder))
            # Covariance with dashed outline
            cir = Circle((0,0), 1, edgecolor=self.colors[idx], fill=False, linestyle='--', linewidth=6,
                         transform=self.get_undistort_trans(posx=1e4, posy=1e4),
                         zorder=self.belief_cov_zorder,
                         alpha=0.5)
            self.cov_handles.append(cir)
            self.ax.add_artist(cir)

        # Use scatter plot as the mean of the estimates
        self.mean_handles = self.ax.scatter([1e4]*len(self.patches), [1e4]*len(self.patches), s=300, c=self.colors, marker='x',
                                            zorder=self.belief_mean_zorder, alpha=1, linewidth=10)

        # Edges for showing detections, constructed as measurements come in
        self.detection_lines = {}
        self.detection_lines_timeout_counter = {}

        # Get robot patches
        robot_patch_dir = join(dir, "robot_patches")
        self.uav_patch = image.imread(join(robot_patch_dir, "uav_k.png"))
        self.ugv_patch = image.imread(join(robot_patch_dir, "ugv_tri_right.png"))
        self.robot_patch_handles = dict()  # To be initialized upon receiving messages
        self.known_uav_types = ['HX', 'SQ']
        self.known_ugv_types = ['TB']

        # Create a new timer object. Set the interval to 100 milliseconds
        # (1000 is default) and tell the timer what function should be called.
        self.timer = self.fig.canvas.new_timer(interval=self.timer_interval_ms)
        self.timer.add_callback(self.update)


        self.sig_handle = signal(SIGINT, self.interrupt_shutdown)

    def start(self):
        self.timer.start()
        # Have to be after timer start() for some reason..
        plt.show()

    def interrupt_shutdown(self, signal_received, frame):
        print("SIGINT or CTRL-C detected. Close fig and exit.")
        self.shutdown()

    def shutdown(self):
        if self.fig:
            plt.close(self.fig)
        print("Shutdown.")
        exit(0)


    def get_undistort_trans(self, posx, posy, theta=0):
        """
        posx: x position in pixels
        posy: y position in pixels
        theta: angle in radians
        """
        # Assume that the image patch is already centered around origin and its size is in meters (before undistortion)
        return transforms.Affine2D().rotate(theta)\
                    .scale(self.xscale, self.yscale)\
                    .translate(posx, posy) + self.ax.transData

    def get_undistort_trans_cov(self, posx, posy, theta, width, height):
        return transforms.Affine2D().scale(width, height).rotate(theta)\
                    .scale(self.xscale, self.yscale)\
                    .translate(posx, posy) + self.ax.transData

    def update(self):


        for idx, (world_xy, target_patch_handle) in enumerate(zip(self.fixed_xy_positions, self.patch_handles)):
            target_xy = self.converter.toProjPos(np.array(world_xy))
            target_patch_handle.set_transform(self.get_undistort_trans(target_xy[0], target_xy[1], theta=0))
            self.target_groundtruth_xys[idx] = target_xy

        self.ax.figure.canvas.draw()

    def get_cov_width_height_yaw(self, cov, confidence=0.99):
        """
        Draw a covariance ellipse around a particular pose.
        :param pose: The center of the ellipse.
        :param cov: The covariance matrix to plot.
        :param clr: The color to plot.
        :param confidence: The confidence on [0, 1].
        :return: The resulting patch added to the figure.
        """
        # For 2D confidence, we can use the inverse of Chi-Square with 2 DOF
        s = -2 * np.log(1 - confidence)  # Confidence Parameter: s
        eig_val, eig_vec = np.linalg.eig(cov)
        width = 2 * np.sqrt(s * eig_val[0])
        height = 2 * np.sqrt(s * eig_val[1])
        yaw = np.arctan(eig_vec[0][1] / eig_vec[0][0])

        return width, height, yaw


if __name__ == '__main__':
    # IP_ADDRESS = "18.47.0.217"
    # IP_PORT = 21569
    # IP_ADDRESS = "127.0.0.1"
    # IP_PORT = 21569

    # THOR
    IP_ADDRESS = "192.168.0.141"
    IP_PORT = 21569
    plotter = TrackingPlotter(server_ip=IP_ADDRESS, server_port=IP_PORT, scale=1.0)
    plotter.start()

