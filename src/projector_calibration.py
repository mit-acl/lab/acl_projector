#!/usr/bin/env python
from __future__ import division

import matplotlib as mpl
mpl.use("TkAgg")
mpl.rcParams['toolbar'] = 'None'  # Disable toolbar
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import numpy as np
import time
import sys
import pickle
from signal import signal, SIGINT
import udputil


MOUSE_LEFT = 1
MOUSE_WHEEL = 2
MOUSE_RIGHT = 3


class Canvas(object):


    def __init__(self, server_ip, server_port, num_per_row=5, num_per_col=5, proj_size_pixels=(2400, 3840),
                 cross_size=100, cross_edge_width=5, circle_size=100, text_fontsize=20,
                 dpi=mpl.rcParams["figure.dpi"], timer_interval_ms=50, scale=1.0,
                 calibration_file_name="calibration"):

        self.sig_handle = signal(SIGINT, self.interrupt_shutdown)

        self.calibration_file_name = calibration_file_name
        self.num_per_row = num_per_row
        self.num_per_col = num_per_col
        self.cross_size = cross_size  # size of the "+" marker on the grid
        self.cross_edge_width = cross_edge_width
        self.circle_size = circle_size  # size of the circle patch showing the current marker
        self.text_fontsize = text_fontsize

        # Set up UDP functionalities
        self.server_ip = server_ip
        self.server_port = server_port
        self.udp = udputil.UdpReceiver(self.server_ip, self.server_port)

        # Position convertion
        # self.converter = projutil.PosConverter('calibration_data_half_bay.p')

        # Canvas
        self.proj_size_pixels = proj_size_pixels  #(width, height)
        self.dpi = dpi
        self.scale = scale
        self.size_in_inches = (self.scale*self.proj_size_pixels[0]/self.dpi, self.scale*self.proj_size_pixels[1]/self.dpi)
        self.fig, self.ax = plt.subplots(figsize=self.size_in_inches)
        self.ax.set_xlim([0,self.proj_size_pixels[0]])
        self.ax.set_ylim([0,self.proj_size_pixels[1]])
        self.timer_interval_ms = timer_interval_ms

        self.ax.set_aspect('equal')
        # self.ax.invert_yaxis()

        # https://www.onooks.com/remove-all-whitespace-from-borders-in-matplotlib-imshow/
        # remove margins & all the white space surrounding axes
        plt.subplots_adjust(0,0,1,1,0,0)
        self.ax.margins(0,0)
        self.ax.xaxis.set_major_locator(plt.NullLocator())
        self.ax.yaxis.set_major_locator(plt.NullLocator())

        # Set the window location at top left for TkAgg: https://newbedev.com/how-do-you-set-the-absolute-position-of-figure-windows-with-matplotlib
        self.mngr = plt.get_current_fig_manager()
        self.mngr.window.wm_geometry("+0+0")

        # Remove window frame: https://www.py4u.net/discuss/221485
        self.fig.canvas.manager.window.overrideredirect(1)

        # Create a new timer object. Set the interval to 100 milliseconds
        # (1000 is default) and tell the timer what function should be called.
        self.timer = self.fig.canvas.new_timer(interval=self.timer_interval_ms)
        # self.timer.add_callback(update, self)
        self.timer.add_callback(self.update)
        self.timer.start()

        self.grid_pos_list = self.gen_grid_pos_list()  # Place "+" marker for where to place the wand
        self.vicon_pos_list = np.zeros(self.grid_pos_list.shape)

        self.crosses = plt.plot(self.grid_pos_list[:,0], self.grid_pos_list[:,1], marker="+",
                                markersize=self.cross_size, markeredgewidth=self.cross_edge_width, color='k', linestyle='none')

        # Circle for marking where the wand should be placed
        self.curr_marker_idx = 0
        self.current_marker = Circle(self.grid_pos_list[self.curr_marker_idx],
                                     self.circle_size, edgecolor='r', fill=False, lw=4)
        self.ax.add_artist(self.current_marker)

        self.current_vicon_pos = np.zeros(2)

        # Text for each grid location:
        self.text_handles = [None]*self.grid_pos_list.shape[0]
        y_offset_pixels = proj_size_pixels[1]/(self.num_per_col+1)/5
        for idx, pos in enumerate(self.grid_pos_list):
            text_str = self.get_text(self.current_vicon_pos, pos)
            # self.text_handles[idx]=self.ax.text(pos[0]/proj_size_pixels[0], (pos[1]-y_offset_pixels)/proj_size_pixels[1],
            #          text_str, horizontalalignment='center', verticalalignment='center', transform=self.ax.transAxes,
            #          fontsize=self.text_fontsize)
            self.text_handles[idx]=self.ax.text(pos[0], (pos[1]-y_offset_pixels),text_str, fontsize=self.text_fontsize)
            self.ax.add_artist(self.text_handles[idx])
            # self.text_handles.append(text_handle)

        # Register mouse activity
        self.fig.canvas.mpl_connect("button_press_event", self.onclick)

        plt.show()


    def onclick(self, event):
        print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
          ('double' if event.dblclick else 'single', event.button,
           event.x, event.y, event.xdata, event.ydata))
        if event.button==MOUSE_LEFT:
            # Log current wand location and proceed to next
            self.vicon_pos_list[self.curr_marker_idx] = self.current_vicon_pos
            txt = self.get_text(self.vicon_pos_list[self.curr_marker_idx],self.grid_pos_list[self.curr_marker_idx])
            print(txt)
            txt_h = self.text_handles[self.curr_marker_idx]
            print(txt_h)
            txt_h.set_text(txt)
            print(self.text_handles[self.curr_marker_idx])
            self.text_handles[self.curr_marker_idx].text = self.get_text(self.vicon_pos_list[self.curr_marker_idx],
                                                                    self.grid_pos_list[self.curr_marker_idx])
            # Determine whether to save the calibration data and exit
            if (self.curr_marker_idx+1) == self.vicon_pos_list.shape[0]:
                self.save_and_quit()

            # Increment
            self.curr_marker_idx += 1
            
        elif event.button==MOUSE_RIGHT:
            # Back to previous location (clear the current data)
            self.vicon_pos_list[self.curr_marker_idx] = [0, 0]
            self.text_handles[self.curr_marker_idx].set_text(self.get_text(self.vicon_pos_list[self.curr_marker_idx],
                                                                    self.grid_pos_list[self.curr_marker_idx]))
            # Decrement
            self.curr_marker_idx = max(0, self.curr_marker_idx-1)



        # Update the visualization
        self.current_marker.center = self.grid_pos_list[self.curr_marker_idx,0], self.grid_pos_list[self.curr_marker_idx,1]
        self.fig.canvas.draw()


    def get_text(self, vicon_pos, grid_pos):
        return "grid=[{},{}]\nvicon=[{:.2f},{:.2f}]".format(grid_pos[0], grid_pos[1], vicon_pos[0], vicon_pos[1])

    def save_and_quit(self):
        pickle_dict = dict()
        pickle_dict['size'] = self.proj_size_pixels
        pickle_dict['proj_grid'] = self.grid_pos_list
        pickle_dict['vicon_grid'] = self.vicon_pos_list

        filename = '{}.p'.format(self.calibration_file_name)
        with open(filename, 'wb') as f:
            pickle.dump(pickle_dict, f)
        print("Saved to {}".format(filename))

        # CLose the window
        plt.close(self.fig)


    def update(self):
        # Grab Wand location and update current_vicon_pos internally
        # Listen for wand location
        if self.udp.update():
            udpDict = self.udp.get_latest_data()
            if 'WAND' in udpDict.keys():
                udp_msg = udpDict['WAND'].data
                self.current_vicon_pos = udp_msg['pose']['position'][:2]
                print('wand world location: {}'.format(self.current_vicon_pos))
            else:
                print("No wand data")

        # self.ax.figure.canvas.draw()


    def gen_grid_pos_list(self):
        # # Return a list of grid points as a N-by-2 numpy array (in pixels)
        # xskip = round(self.proj_size_pixels[0]/(self.num_per_row+1))
        # yskip = round(self.proj_size_pixels[1]/(self.num_per_col+1))
        # xvalues = np.linspace(xskip, self.proj_size_pixels[0]-xskip, self.num_per_row, endpoint=True)
        # yvalues = np.linspace(yskip, self.proj_size_pixels[1]-yskip, self.num_per_col, endpoint=True)
        # xx, yy = np.meshgrid(xvalues, yvalues)
        # pos_list = np.vstack([np.reshape(xx,xx.size),np.reshape(yy, yy.size)]).T

        # Return a list of grid points as a N-by-2 numpy array (in pixels)
        margin_pixels = 200
        xvalues = np.linspace(margin_pixels, self.proj_size_pixels[0]-margin_pixels, self.num_per_row, endpoint=True)
        yvalues = np.linspace(margin_pixels, self.proj_size_pixels[1]-margin_pixels, self.num_per_col, endpoint=True)
        xx, yy = np.meshgrid(xvalues, yvalues)
        pos_list = np.vstack([np.reshape(xx,xx.size),np.reshape(yy, yy.size)]).T

        return pos_list

    def interrupt_shutdown(self, signal_received, frame):
        if self.fig:
            plt.close(self.fig)
        print("SIGINT or CTRL-C detected. Close fig and exit.")
        exit(0)


if __name__ == '__main__':
    # IP_ADDRESS = "18.47.0.217"
    # IP_PORT = 21569
    # IP_ADDRESS = "127.0.0.1"
    # IP_PORT = 21569

    # THOR
    IP_ADDRESS = "192.168.0.141"
    IP_PORT = 21569
    #
    calibration_file_name = "calibration_left"
    plotter = Canvas(server_ip=IP_ADDRESS, server_port=IP_PORT, scale=1,
                     calibration_file_name=calibration_file_name)