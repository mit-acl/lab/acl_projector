class config:
    IP_ADDRESS         = "18.47.0.217"
    IP_PORTS           = [21569, 21542, 21548, 21592, 21501, 21582, 21529, 21578]
    PROJ_SIZE          = (7200, 3840)
    COLORS             = ['r', 'g', 'b']

    MAX_RING_RADIUS    = 150.0
    RING_INV_RATE      = 40.0
    RING_RADIUS        = 100.0
    RING_WIDTH         = 25.0

    GOAL_SCALE         = [0.15, 0.20]

    VEL_SCALE          = 400
    VEL_TRAJ_NUM       = 15
    VEL_TRAJ_SIZE      = 45.
    MAX_VEL            = 50
    VEL_INV_RATE       = 40

    HELP_RADIUS_SQ     = 100**2
    DOWNSCALE          = 0.97
    HELP_COUNT         = 30
    HELP_SCALE         = [0.60, 0.80]
